#include "buffers.h"
#include "calibration.h"

uint32_t ADC_DMA_BUFFER[BUFFER_SIZE];
float PROCESSING_BUFFER[BUFFER_SIZE];
uint32_t DAC_DMA_BUFFER[BUFFER_SIZE];

int WHICH_HALF = 0;

void ADC_buffer_to_processing_buffer()
{
	int offset = WHICH_HALF == 0 ? 0 : HALF_BUFFER_SIZE;

	for (int i = 0; i < HALF_BUFFER_SIZE; i++)
	{
		PROCESSING_BUFFER[i + offset] = (ADC_DMA_BUFFER[i + offset] / 4096.0f
				- 0.5f) * 2 - EMPTY_SIGNAL_AVG;
	}
}

void processing_buffer_to_DAC_buffer()
{
	int offset = WHICH_HALF == 0 ? HALF_BUFFER_SIZE : 0;

	for (int i = 0; i < HALF_BUFFER_SIZE; i++)
	{
		DAC_DMA_BUFFER[i + offset] = (PROCESSING_BUFFER[i + offset] / 2 + 0.5f)
				* 4096.0f;
	}
}

