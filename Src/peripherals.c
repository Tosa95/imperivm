#include "peripherals.h"
#include "buffers.h"

void start_converters()
{
	HAL_TIM_Base_Start(&htim2);
	HAL_ADC_Start_DMA(&hadc1, ADC_DMA_BUFFER, BUFFER_SIZE);
	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, DAC_DMA_BUFFER, BUFFER_SIZE,
			DAC_ALIGN_12B_R);
}
