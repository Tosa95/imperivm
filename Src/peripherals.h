#ifndef PERIPHERALS_H_
#define PERIPHERALS_H_

#include "stm32f4xx_hal.h"

extern ADC_HandleTypeDef hadc1;
extern DAC_HandleTypeDef hdac;
extern TIM_HandleTypeDef htim2;
extern SPI_HandleTypeDef hspi2;

void start_converters();

#endif /* PERIPHERALS_H_ */
