#include "calibration.h"

float compute_avg(float *vect, int size)
{
	float avg = 0;

	for (int i = 0; i < size; i++)
	{
		avg += vect[i];
	}

	avg /= size;

	return avg;
}


float multiple_avg(float *vect, int size)
{
	static int i = 0;
	static float old_avg = 0;

	float avg = compute_avg(vect, size);

	if (i==0)
	{
		old_avg = avg;
	}
	else
	{
		old_avg = (old_avg*i + avg)/(1+i);
	}

	i++;

	if (i > KEEP_AVERAGE_FACTOR)
	{
		i = 0;
	}

	return old_avg;
}
