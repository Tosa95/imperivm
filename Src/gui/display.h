/*
 * display_test.h
 *
 *  Created on: Aug 11, 2019
 *      Author: Davide
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "main.h"

void initDisplay ();
void testDisplay();
void refreshDisplay();
void setPixelDisplay (int x, int y, char color);

#endif /* DISPLAY_H_ */
