/*
 * display_test.c
 *
 *  Created on: Aug 11, 2019
 *      Author: Davide
 */

#include "display.h"
#include "lvgl/lvgl.h"

#include "stdint.h"
#include "../peripherals.h"

#define A0_COMMAND GPIO_PIN_RESET
#define A0_DATA GPIO_PIN_SET

#define CS_SELECTED GPIO_PIN_RESET
#define CS_NOT_SELECTED GPIO_PIN_SET

#define RESET_ON GPIO_PIN_RESET
#define RESET_OFF GPIO_PIN_SET

#define MAXX 128
#define MAXY 64

#define PAGE_NUM 8

//Buffer grafico: contiene lo stato attuale di tutti i pixel del display
static uint8_t displayMtrx [PAGE_NUM][MAXX];
static lv_disp_buf_t disp_buf;

static void flush_cb(lv_disp_drv_t * disp_drv, const lv_area_t * area, lv_color_t * color_p);
static void set_px_cb(lv_disp_drv_t * disp_drv, uint8_t * buf, lv_coord_t buf_w, lv_coord_t x, lv_coord_t y, lv_color_t color, lv_opa_t opa);

static void initMtrx()
{
    int i, j;

    for (i = 0; i < PAGE_NUM; i++)
    {
        for (j = 0; j < MAXX; j++)
        {
            displayMtrx[i][j] = 0;
        }
    }
}

static inline void sendCommand(uint8_t command)
{
	HAL_GPIO_WritePin(DISP_A0_GPIO_Port, DISP_A0_Pin, A0_COMMAND);
	HAL_SPI_Transmit(&hspi2, &command, 1, 100);
}

static inline void sendData(uint8_t data)
{
	HAL_GPIO_WritePin(DISP_A0_GPIO_Port, DISP_A0_Pin, A0_DATA);
	HAL_SPI_Transmit(&hspi2, &data, 1, 100);
}

static inline void chipSelecOn()
{
	HAL_GPIO_WritePin(DISP_CS_GPIO_Port, DISP_CS_Pin, CS_SELECTED);
}

static inline void chipSelecOff()
{
	HAL_GPIO_WritePin(DISP_CS_GPIO_Port, DISP_CS_Pin, CS_NOT_SELECTED);
}

static inline void reset()
{
	HAL_GPIO_WritePin(DISP_RES_GPIO_Port, DISP_RES_Pin, RESET_ON);
	HAL_Delay(100);
	HAL_GPIO_WritePin(DISP_RES_GPIO_Port, DISP_RES_Pin, RESET_OFF);
	HAL_Delay(100);
}

static inline void setPosition (uint8_t x, uint8_t y)
{
    char com1, com2, com3;

    com1 = (0x0B<<4) | (y & 0x0F);
    com2 = (0x00<<4) | (x & 0x0F);
    com3 = (0x01<<4) | ((x & 0xF0)>>4);

    sendCommand (com1);
    sendCommand (com2);
    sendCommand (com3);
}

void initDisplay ()
{
	//HAL_Delay(500);
	reset();

	chipSelecOn();
	//HAL_Delay(100);
    sendCommand (0xA2);
    //HAL_Delay(100);
    sendCommand (0xA0);
    //HAL_Delay(100);
    sendCommand (0xC8);
    //HAL_Delay(100);
    sendCommand (0xA4);
    //HAL_Delay(100);
    sendCommand (0x40);
    //HAL_Delay(100);
    sendCommand (0x25);
    //HAL_Delay(100);
    sendCommand (0x81);
    //HAL_Delay(100);
    sendCommand (0x13);
    //HAL_Delay(100);
    sendCommand (0x2F);
    //HAL_Delay(100);
    sendCommand (0xAF);
    //HAL_Delay(100);
    sendCommand(0xA3);
    //HAL_Delay(100);

    // Sets the bias, without this the display works but the image is not clear
    sendCommand(0b10100010);
    //HAL_Delay(100);

    // The following two commands set the electronic volume. The higher it is, the darker will be the pixels.
    sendCommand(0b10000001);
    //HAL_Delay(100);
    sendCommand(0b00100000);
    //HAL_Delay(100);

//    sendCommand(0b00100000);
//    HAL_Delay(20);

//    sendCommand (0xF8);
//    sendCommand (0x03);

    initMtrx();
    refreshDisplay();

    lv_init();
    lv_disp_buf_init(&disp_buf, displayMtrx, NULL, MAXX*MAXY);
    lv_disp_drv_t disp_drv;                 /*A variable to hold the drivers. Can be local variable*/
	lv_disp_drv_init(&disp_drv);            /*Basic initialization*/
	disp_drv.buffer = &disp_buf;            /*Set an initialized buffer*/
	disp_drv.flush_cb = flush_cb;        /*Set a flush callback to draw to the display*/
	disp_drv.set_px_cb = set_px_cb;
	lv_disp_t * disp;
	disp = lv_disp_drv_register(&disp_drv); /*Register the driver and save the created display objects*/
}

void clearDisplay()
{
	for (int i = 0; i < PAGE_NUM; i++)
	{
		setPosition(0, i);
		for (int j = 0; j < MAXX; j++)
		{
			sendData (0x00);
		}
	}
}

void testDisplay()
{
	for (int i = 0; i < PAGE_NUM; i++)
	{
		setPosition(0, i);
		for (int j = 0; j < MAXX; j++)
		{
			if (i%2 == 0)
			{
				sendData (0xFF);
			}
			else
			{
				sendData (0x00);
			}

		}
	}

}

void refreshDisplay ()
{
    int i, j;

    for (i = 0; i < PAGE_NUM; i++)
    {
        setPosition (0, i);
        for (j = 0; j < MAXX; j++)
        {
            sendData (displayMtrx[i][j]);
        }
    }
}

void setPixelDisplay (int x, int y, char color)
{
    char page = (char)y>>3, data, bitNum = (char)y%8, mask;

    if (x < MAXX && y < MAXY && x >= 0 && y >= 0)
    {
        data = displayMtrx[page][x];

        mask = 0x01<<bitNum;

        if (color != 0)
        {
            data |= mask;
        }
        else
        {
            mask = ~mask;
            data &= mask;
        }

        displayMtrx[page][x] = data;
    }
}

static void flush_cb(lv_disp_drv_t * disp_drv, const lv_area_t * area, lv_color_t * color_p)
{
	//refreshDisplay();
	lv_disp_flush_ready(disp_drv);
}

static void set_px_cb(lv_disp_drv_t * disp_drv, uint8_t * buf, lv_coord_t buf_w, lv_coord_t x, lv_coord_t y, lv_color_t color, lv_opa_t opa)
{
	uint8_t col = lv_color_to1(color);
    setPixelDisplay(x, y, col==1?0:1);
}
