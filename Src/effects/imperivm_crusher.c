/*
 * IMPERIVM
 *
 *  Created on: 18 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "imperivm_crusher.h"

#include "math.h"
#include "main.h"
#include "clip.h"

typedef struct{
	float old_sample;
	int remaining_time;
}crusher_data;

effect_t *init_imperivm_crusher(effect_t *effect)
{
	strcpy(effect->name, "IMPERIVM Crush");

	effect->process = &imperivm_crusher;

	set_effect_base_param(effect, IMPERIVM_CRUSHER_LEVELS, IMPERIVM_CRUSHER_LEVELS_DEFAULT, 4, 256, 1, "L");
	set_effect_base_param(effect, IMPERIVM_CRUSHER_DOWNSAMPLING, IMPERIVM_CRUSHER_DOWNSAMPLING_DEFAULT, 0.01, 1, 0.01, "D");
	set_effect_base_param(effect, IMPERIVM_CRUSHER_MIX, IMPERIVM_CRUSHER_MIX_DEFAULT, 0, 1, 0.01, "M");

	effect->bypass = IMPERIVM_CRUSHER_BYPASS;

	effect->other_data = (crusher_data*)malloc(sizeof(crusher_data));

	crusher_data *cd = (crusher_data*)effect->other_data;
	cd->old_sample = 0;
	cd->remaining_time = 0;
}

void imperivm_crusher(effect_t *data, float *buffer, uint32_t size)
{
	float levels = data->params[IMPERIVM_CRUSHER_LEVELS];
	float downsampling = data->params[IMPERIVM_CRUSHER_DOWNSAMPLING];
	float mix = data->params[IMPERIVM_CRUSHER_MIX];

	float remaining_time_max = 1.0/downsampling;

	crusher_data *cd = (crusher_data*)data->other_data;

	float x = cd->old_sample;

	for(int i = 0; i < size; i++)
	{
		if (cd->remaining_time <= 0)
		{
			x = buffer[i];

			x = (x + 1) * 0.5;
			x = ((int)round(x * levels))/levels;
			x = x*2 - 1;

			cd->old_sample = x;
			cd->remaining_time = (int)remaining_time_max;
		}

		cd->remaining_time--;


		buffer[i] = (1-mix) * buffer[i] + mix * x;
	}

}


