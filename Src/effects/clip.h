/*
 * IMPERIVM
 *
 *  Created on: 16 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_CLIP_H_
#define EFFECTS_CLIP_H_

#include "stdint.h"
#include "chain.h"

#define CLIP_CUT_AT 0
#define CLIP_MAX 1
#define CLIP_BRUTALITY 2

#define CLIP_CUT_AT_DEFAULT 0.95
#define CLIP_MAX_DEFAULT 1
#define CLIP_BRUTALITY_DEFAULT 2

#define PARAMETRIC_CLIP_MAX_ESTIMATION_DERIVATIVE_STEP 0.001
#define PARAMETRIC_CLIP_MAX_ESTIMATION_DERIVATIVE_TH 0.01
#define PARAMETRIC_CLIP_MAX_ESTIMATION_LR 2
#define PARAMETRIC_CLIP_MAX_ESTIMATION_LR_CORRECTION_FAC 5
#define PARAMETRIC_CLIP_MAX_ESTIMATION_MAX_IT 1000
#define PARAMETRIC_CLIP_MAX_ESTIMATION_MAX_CORRECTION_FACTOR 1.005

void init_clip(effect_t *effect);
void clip_single_sample(float clip_at, float *sample);
void parametric_clip_single_sample(float clip_at, float clip_factor, float brutality, float *sample);
float parametric_clip_get_correction_factor(float clip_at, float clip_factor, float brutality);
void soft_clip_single_sample(float clip_at, float *sample);
void softer_clip_single_sample(float clip_at, float *sample);
void clip(effect_t *data, float *buffer, uint32_t size);

#endif /* EFFECTS_CLIP_H_ */
