/*
 * IMPERIVM
 *
 *  Created on: 27 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "lfo.h"
#include "math.h"
#include "signals.h"
#include <string.h>


float lfo(float min_value, float amplitude, float freq, float t, int wave_shape)
{
	float wave_value = 0;

	wave_shape = (int)wave_shape;

	if(wave_shape == LFO_WAVE_SIN)
	{
		wave_value = 0.5 + 0.5 * sinf(2 * M_PI * freq * t);
	}

	if(wave_shape == LFO_WAVE_SIN4)
	{
		wave_value = - powf(sinf(M_PI * freq * t), 4) + 1;
	}

	if(wave_shape == LFO_WAVE_TRI)
	{
		float period = 1.0/freq;
		wave_value = trif(t, period);
	}

	return min_value + amplitude * wave_value;

}

void lfoWaveToString(float param, param_data_t *data, char *result)
{
	int wave_shape = (int)param;

	if(wave_shape == LFO_WAVE_SIN)
	{
		strcpy(result,"S");
	}

	if(wave_shape == LFO_WAVE_SIN4)
	{
		strcpy(result,"S4");
	}

	if(wave_shape == LFO_WAVE_TRI)
	{
		strcpy(result,"T");
	}
}

