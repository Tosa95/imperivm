/*
 * IMPERIVM
 *
 *  Created on: 31 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_OVERDRIVE_H_
#define EFFECTS_OVERDRIVE_H_


#include "stdint.h"
#include "chain.h"

#define OVERDRIVE_GAIN 0
#define OVERDRIVE_CLIP_AT 1

#define OVERDRIVE_GAIN_DEFAULT 2
#define OVERDRIVE_CLIP_AT_DEFAULT 0.95

// OVERDRIVE_GAIN_DEFAULT from 2/3 to 15
//	- 2/3 no clipping, gently boosted signal
//	- 2 FUN
// 	- 15 evilly overdriven signal (it's practically a distortion)


void init_overdrive(effect_t *effect);
void overdrive(effect_t *data, float *buffer, uint32_t size);


#endif /* EFFECTS_OVERDRIVE_H_ */
