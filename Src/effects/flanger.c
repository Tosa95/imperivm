/*
 * IMPERIVM
 *
 *  Created on: 27 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "math.h"
#include "main.h"
#include "flanger.h"
#include "past.h"
#include "global_constants.h"
#include "clip.h"
#include "string.h"

#define LEVEL_KEEP_BUFFERS 50000 // for the moving average
#define FEEDBACK_BUFFER_SIZE 5000
#define FLANGER_MIN_DELAY 0.00052 // This paremeter is fundamental on the sound that is generated, we have to fine tune it!

#define COMPUTE_LFO_EVERY_SAMPLES 0

float feedback_buffer[FEEDBACK_BUFFER_SIZE];

typedef struct {
	float t; // LFO time
	float old_level; // for the moving average level adjust
	int curr_index; // current index of the feedback buffer
	int lfo_recompute_in;
	int old_delay_seconds;
	int old_delay_samples;
} flanger_data;

void init_flanger(effect_t *effect)
{
	strcpy(effect->name, "Flanger");

	effect->process = &flanger;

	set_effect_base_param(effect, FLANGER_SPEED, FLANGER_SPEED_DEFAULT, 0.1, 1, 0.02, "S");
	set_effect_base_param(effect, FLANGER_DEPTH, FLANGER_DEPTH_DEFAULT, 0.0005, 0.004, 0.0001, "D");
	set_effect_base_param(effect, FLANGER_REGEN, FLANGER_REGEN_DEFAULT, 0, 0.9, 0.02, "R");
	set_effect_base_param(effect, FLANGER_MIX, FLANGER_MIX_DEFAULT, 0.1, 1, 0.02, "M");
	set_effect_base_param(effect, FLANGER_WAVE, FLANGER_WAVE_DEFAULT, 0, LFO_WAVES_NUM-0.0001, 1.0/3.0, "W");
	effect->params_data[FLANGER_WAVE].toString=lfoWaveToString;
	effect->params_data[FLANGER_WAVE].change=circularParamChange;

	effect->bypass = FLANGER_BYPASS;

	effect->other_data = malloc(sizeof(flanger_data));

	flanger_data *fd = (flanger_data *)effect->other_data;

	fd->t = 0;
	fd->curr_index = 0;
	fd->old_level = 1;
	fd->lfo_recompute_in = COMPUTE_LFO_EVERY_SAMPLES;
	fd->old_delay_samples = -1;
	fd->old_delay_seconds = -1;

	for (int i = 0; i < FEEDBACK_BUFFER_SIZE; i++)
	{
		feedback_buffer[i] = 0;
	}
}

void flanger(effect_t *data, float *buffer, uint32_t size)
{
	float speed = data->params[FLANGER_SPEED];
	float depth = data->params[FLANGER_DEPTH];
	float regen = data->params[FLANGER_REGEN];
	float mix = data->params[FLANGER_MIX];
	float wave = data->params[FLANGER_WAVE];

	flanger_data *fd = (flanger_data *)data->other_data;


	// get delay samples number, based on LFO wave value

	float lfo_period = 1.0/speed;

	float delay_seconds;
	int delay_samples;

	int j = get_begin_of_current_block();

	float original_energy = 0;
	float flanged_energy = 0;

	for(int i = 0; i < size; i++)
	{

		// In order to be sure the updates are equispaced in time
		if (fd->lfo_recompute_in == 0)
		{
			delay_seconds = lfo(FLANGER_MIN_DELAY, depth, speed, fd->t, wave);
			delay_samples = round(delay_seconds * SF);
			fd->lfo_recompute_in = COMPUTE_LFO_EVERY_SAMPLES;
			fd->old_delay_samples = delay_samples;
			fd->old_delay_seconds = delay_seconds;
		}
		else
		{
			delay_seconds = fd->old_delay_seconds;
			delay_samples = fd->old_delay_samples;
			fd->lfo_recompute_in--;
		}

//		if (i % 1000 == 0)
//		{
//			delay_seconds = lfo(FLANGER_MIN_DELAY, depth, speed, fd->t, wave);
//			delay_samples = round(delay_seconds * SF);
//			fd->lfo_recompute_in = COMPUTE_LFO_EVERY_SAMPLES;
//		}



		original_energy += buffer[i] * buffer[i];


		// compute the signal value coming out from the delay line

		int feedback_index = get_wrapped_index(fd->curr_index - delay_samples,FEEDBACK_BUFFER_SIZE);

		int feedback_index2 = get_wrapped_index(fd->curr_index - delay_samples - 1000,FEEDBACK_BUFFER_SIZE);

		int feedback_index3 = get_wrapped_index(fd->curr_index - delay_samples - 2000,FEEDBACK_BUFFER_SIZE);

		float delayed = (buffer[i] + regen * feedback_buffer[feedback_index]);

		feedback_buffer[fd->curr_index] = delayed;
		fd->curr_index = get_wrapped_index(fd->curr_index + 1, FEEDBACK_BUFFER_SIZE);

		// compute the overall output value for the sample i

		buffer[i] = (1 - mix) * buffer[i] + mix * feedback_buffer[feedback_index] + mix/2*feedback_buffer[feedback_index2] + mix/3*feedback_buffer[feedback_index3];

		flanged_energy += buffer[i] * buffer[i];

		// update the new t index for the LFO wave

		fd->t += 1.0/SF;

		if(fd->t > lfo_period)
		{
			fd->t = 0;
		}

		j++;
	}

	// adjust the output level

//	float level = sqrtf(original_energy) / sqrtf(flanged_energy);
//
//	fd->old_level = level;
//	level = (level + LEVEL_KEEP_BUFFERS * (fd->old_level)) / (1 + LEVEL_KEEP_BUFFERS);
//
//	float amp = 2/M_PI;
//	float gain = 2.5;

//	for(int i = 0; i < size; i++)
//	{
//		buffer[i] *= 1;

		// This is to be sure to avoid having values over 1.
		// Soft clipping function has been created genetically in python.
		// Since it is impossible to force the flanger effect to stay in the range -1 1 since
		// it is the sum of two signals in the same range (even if mixed they could eventually
		// go over 1), the soft clipping function forces the samples to stay inside the right range.
		// It is strictly linear up to 0.9, then it gently cuts to 1 with no discontinuities, also derivative is
		// preserved.
		//soft_clip_single_sample(0.98, &(buffer[i]));
		//softer_clip_single_sample(0.95, &(buffer[i]));
		//buffer[i] = atanf(gain * buffer[i])*amp;
//	}



}


