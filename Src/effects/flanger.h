/*
 * IMPERIVM
 *
 *  Created on: 27 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_FLANGER_H_
#define EFFECTS_FLANGER_H_


#include "stdint.h"
#include "chain.h"
#include "lfo.h"

#define FLANGER_SPEED 0
#define FLANGER_DEPTH 1
#define FLANGER_REGEN 2
#define FLANGER_MIX 3
#define FLANGER_WAVE 4

#define FLANGER_SPEED_DEFAULT 0.5
#define FLANGER_DEPTH_DEFAULT 0.001
#define FLANGER_REGEN_DEFAULT 0.7
#define FLANGER_MIX_DEFAULT 0.8
#define FLANGER_WAVE_DEFAULT LFO_WAVE_SIN

// FLANGER_SPEED_DEAFAULT from 0.1 to 1 Hz
//	- 0.5 FUN

// FLANGER_DEPTH_DEAFAULT from 0.0005 to 0.0015 s
//	- 0.0005 gives a soft effect
//	- 0.001 FUN
//	- 0.005 begins to be out of tune

// FLANGER_REGEN_DEAFAULT from 0 to 1
// 	- 0 no flanger feedback
//	- 0.7 FUN
//	- 1 high flanger feedback

// FLANGER_MIX_DEAFAULT from 0.1 to 1
//	- 0 only dry signal
//	- 0.7 FUN
// 	- 1 only flanged signal

// FLANGER_WAVE_DEFAULT
//	- LFO_WAVE_SIN or LFO_WAVE_SIN4 are FUN


void init_flanger(effect_t *effect);
void flanger(effect_t *data, float *buffer, uint32_t size);


#endif /* EFFECTS_FLANGER_H_ */
