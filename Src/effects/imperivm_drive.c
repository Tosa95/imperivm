#include "imperivm_drive.h"
#include "math.h"
#include "main.h"
#include "clip.h"
#include "global_constants.h"

#define DYNAMIC_GAIN_CORRECTION 0.008
#define LEVEL_KEEP_BUFFERS 200
#define GAIN_KEEP_BUFFERS 200
#define DEFAULT_OLD_LEVEL 0.25
#define MAX_DYN_GAIN 300

typedef struct {
	float old_level;
	float old_dyn_gain;
	float old_gain;
	int gain_has_changed;
	float clip_correct_fac;
} imperivm_drive_data;

void init_imperivm_drive(effect_t *effect)
{
	strcpy(effect->name, "IMPERIVM Drive");
	effect->process = &imperivm_drive;

	set_effect_base_param(effect, IMPERIVM_DRIVE_GAIN, IMPERIVM_DRIVE_GAIN_DEFAULT, 1, 30, 0.2, "G");
	effect->params_data[IMPERIVM_DRIVE_GAIN].toString = keepTheSameParamToString;
	set_effect_base_param(effect, IMPERIVM_DRIVE_BRUTALITY, IMPERIVM_DRIVE_BRUTALITY_DEFAULT, 0.1, 3, 0.025, "B");
	set_effect_base_param(effect, IMPERIVM_DRIVE_CLIP_FACTOR, IMPERIVM_DRIVE_CLIP_FACTOR_DEFAULT, -0.4, 0.4, 0.02, "CF");
	set_effect_base_param(effect, IMPERIVM_DRIVE_DYNAMIC, IMPERIVM_DRIVE_DYNAMIC_DEFAULT, 0.0, 1, 0.01, "D");
	set_effect_base_param(effect, IMPERIVM_DRIVE_ADJUST_LEVEL, IMPERIVM_DRIVE_ADJUST_LEVEL_DEFAULT, 0, 1, 0.01, "AL");
	set_effect_base_param(effect, IMPERIVM_DRIVE_CLIP_AT, IMPERIVM_DRIVE_CLIP_AT_DEFAULT, 0.9, 1, 0.01, "CA");

	effect->bypass = IMPERIVM_DRIVE_BYPASS;

	effect->other_data = malloc(sizeof(imperivm_drive_data));

	imperivm_drive_data *idd = (imperivm_drive_data *)effect->other_data;

	idd->old_level = DEFAULT_OLD_LEVEL;
	idd->old_dyn_gain = IMPERIVM_DRIVE_GAIN_DEFAULT;
	idd->old_gain = DEFAULT_OLD_LEVEL;
	idd->gain_has_changed = 1;
	idd->clip_correct_fac = 1;
}

void imperivm_drive(effect_t *data, float *buffer, uint32_t size)
{
	float gain = data->params[IMPERIVM_DRIVE_GAIN];
	float brutality = data->params[IMPERIVM_DRIVE_BRUTALITY];
	float clip_factor = data->params[IMPERIVM_DRIVE_CLIP_FACTOR];
	float dynamic = data->params[IMPERIVM_DRIVE_DYNAMIC];
	float adjust_level = data->params[IMPERIVM_DRIVE_ADJUST_LEVEL];
	float clip_at = data->params[IMPERIVM_DRIVE_CLIP_AT];

	imperivm_drive_data *idd = (imperivm_drive_data *)data->other_data;

	if (data->params_changed)
	{
		idd->clip_correct_fac = parametric_clip_get_correction_factor(clip_at, clip_factor, brutality);
	}

	if (gain != idd->old_gain)
	{
		idd->gain_has_changed = 1;
	}
	idd->old_gain = gain;

	float original_energy = 0;
	float clipped_energy = 0;

	for(int i = 0; i < size; i++)
	{
		original_energy += buffer[i] * buffer[i];
	}

	original_energy = sqrtf(original_energy)/size;

	//float original_gain = gain;

	float dyn_gain = gain / original_energy * DYNAMIC_GAIN_CORRECTION;

	if (!idd->gain_has_changed)
	{
		dyn_gain = (dyn_gain + GAIN_KEEP_BUFFERS*idd->old_dyn_gain)/(1+GAIN_KEEP_BUFFERS);
	}
	else
	{
		//dyn_gain = gain;
	}

	if (dyn_gain > MAX_DYN_GAIN)
	{
		dyn_gain = MAX_DYN_GAIN;
	}

	gain = dynamic * gain + (1 - dynamic) * (dyn_gain);





	original_energy = 0;

	//gain = original_gain;

	for(int i = 0; i < size; i++)
	{

		original_energy += buffer[i] * buffer[i];

		buffer[i] *= gain;

		parametric_clip_single_sample(clip_at, clip_factor, brutality, &(buffer[i]));
		buffer[i] /= idd->clip_correct_fac;
		//clip_single_sample(clip_at, &(buffer[i]));

		clipped_energy += buffer[i] * buffer[i];

	}


	float level = sqrtf(original_energy)/sqrtf(clipped_energy);

	if (/*idd->gain_has_changed == 0 &&*/ original_energy/size > SILENCE_AVG_ENERGY)
	{
		level = (level + LEVEL_KEEP_BUFFERS*idd->old_level)/(1+LEVEL_KEEP_BUFFERS);
	}

	if (original_energy/size > SILENCE_AVG_ENERGY)
	{
		idd->old_level = level;
		idd->old_dyn_gain = dyn_gain;
		idd->gain_has_changed = 0;
	}

	for(int i = 0; i < size; i++)
	{
		buffer[i] *= (level*(adjust_level) + 1*(1-adjust_level));
		//soft_clip_single_sample(0.98, &buffer[i]);
	}



}
