/*
 * IMPERIVM
 *
 *  Created on: 29 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_BOOSTER_H_
#define EFFECTS_BOOSTER_H_


#include "stdint.h"
#include "chain.h"

#define BOOSTER_GAIN 0
#define BOOSTER_CLIP_AT 1

#define BOOSTER_GAIN_DEFAULT 2.5
#define BOOSTER_CLIP_AT_DEFAULT 0.95

// BOOSTER_GAIN_DEFAULT from 1 to 5
//	- 1 nearly the same signal as the input one
//	- 2 FUN
// 	- 5 evilly boosted signal


void init_booster(effect_t *effect);
void booster(effect_t *data, float *buffer, uint32_t size);


#endif /* EFFECTS_BOOSTER_H_ */
