/*
 * IMPERIVM
 *
 *  Created on: 29 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "booster.h"
#include "math.h"
#include "main.h"
#include "global_constants.h"

#define LEVEL_KEEP_BUFFERS 1 // for the moving average

typedef struct {
	float old_level;
} booster_data;

void init_booster(effect_t *effect)
{
	strcpy(effect->name, "Booster");

	effect->process = &booster;

	set_effect_base_param(effect, BOOSTER_GAIN, BOOSTER_GAIN_DEFAULT, 1, 5, 0.2, "G");
	effect->params_data[BOOSTER_GAIN].toString = keepTheSameParamToString;
	set_effect_base_param(effect,  BOOSTER_CLIP_AT,  BOOSTER_CLIP_AT_DEFAULT, 0.9, 1, 0.01, "CA");

	effect->bypass = BOOSTER_BYPASS;

	effect->other_data = malloc(sizeof(booster_data));

	booster_data *bd = (booster_data *)effect->other_data;
	bd->old_level = 1;
}

void booster(effect_t *data, float *buffer, uint32_t size)
{
	float gain = data->params[BOOSTER_GAIN];
	float clip_at = data->params[BOOSTER_CLIP_AT];

	booster_data *bd = (booster_data *)data->other_data;

	float original_energy = 0;
	float boosted_energy = 0;

	// correct the amplification of arc-tangent, such as for x[n] == 1 => y[n] == 1

	float amp =  clip_at / atanf(gain);

	for(int i = 0; i < size; i++)
	{
		original_energy += buffer[i] * buffer[i];

		// perform the boost using arc-tangent function
		buffer[i] = amp * atanf(gain * buffer[i]);

		boosted_energy += buffer[i] * buffer[i];
	}

	// NOTE: the level adjust is currently disabled
	if (original_energy/size > SILENCE_AVG_ENERGY)
	{
		float level = sqrtf(original_energy)/sqrtf(boosted_energy);

		level = (level + LEVEL_KEEP_BUFFERS*bd->old_level)/(1 + LEVEL_KEEP_BUFFERS);

		bd->old_level = level;

		for(int i = 0; i < size; i++)
		{
			// buffer[i] *= level;
		}
	}


}


