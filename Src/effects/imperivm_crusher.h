/*
 * IMPERIVM
 *
 *  Created on: 18 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_IMPERIVM_CRUSHER_H_
#define EFFECTS_IMPERIVM_CRUSHER_H_


#include "stdint.h"
#include "chain.h"

#define IMPERIVM_CRUSHER_LEVELS 0
#define IMPERIVM_CRUSHER_DOWNSAMPLING 1
#define IMPERIVM_CRUSHER_MIX 2

#define IMPERIVM_CRUSHER_LEVELS_DEFAULT 32
#define IMPERIVM_CRUSHER_DOWNSAMPLING_DEFAULT 0.05
#define IMPERIVM_CRUSHER_MIX_DEFAULT 0.99

// IMPERIVM_CRUSHER_LEVELS_DEFAULT from 16 to 256
//	- 16 hard distortion and "plasma" effect
//	- 32 FUN
//	- 256 no levels reduction

// IMPERIVM_CRUSHER_DOWNSAMPLING_DEFAULT from 0.01 to 1
//	- 0.01 hard aliasing, frequency "mistakes"
//	- 0.05 FUN
//	- 1 no downsampling

// IMPERIVM_CRUSHER_MIX_DEFAULT from 0 to 1
//	- 0 only clean signal
//	- 0.98 FUN
//	- 1 only cushed signal

effect_t *init_imperivm_crusher();
void imperivm_crusher(effect_t *data, float *buffer, uint32_t size);


#endif /* EFFECTS_IMPERIVM_CRUSHER_H_ */
