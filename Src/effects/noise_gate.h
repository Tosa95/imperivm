/*
 * IMPERIVM
 *
 *  Created on: 18 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_NOISE_GATE_H_
#define EFFECTS_NOISE_GATE_H_


#include "stdint.h"
#include "chain.h"

#define NOISE_GATE_THRESHOLD 0
#define NOISE_GATE_ACTIVATION_FACTOR 1
#define NOISE_GATE_DECAY_EXP 2

#define NOISE_GATE_THRESHOLD_DEFAULT 0.03
#define NOISE_GATE_ACTIVATION_FACTOR_DEFAULT 55000
#define NOISE_GATE_DECAY_EXP_DEFAULT 1

void init_noise_gate(effect_t *effect);
void noise_gate(effect_t *data, float *buffer, uint32_t size);


#endif /* EFFECTS_NOISE_GATE_H_ */
