#ifndef EFFECTS_IMPERIVM_DRIVE_H_
#define EFFECTS_IMPERIVM_DRIVE_H_

#include "stdint.h"
#include "chain.h"

#define IMPERIVM_DRIVE_GAIN 0
#define IMPERIVM_DRIVE_BRUTALITY 1
#define IMPERIVM_DRIVE_CLIP_FACTOR 2
#define IMPERIVM_DRIVE_DYNAMIC 3
#define IMPERIVM_DRIVE_ADJUST_LEVEL 4
#define IMPERIVM_DRIVE_CLIP_AT 5


#define IMPERIVM_DRIVE_GAIN_DEFAULT 2
#define IMPERIVM_DRIVE_BRUTALITY_DEFAULT 0.3
#define IMPERIVM_DRIVE_CLIP_FACTOR_DEFAULT -0.2
#define IMPERIVM_DRIVE_DYNAMIC_DEFAULT 0.5
#define IMPERIVM_DRIVE_ADJUST_LEVEL_DEFAULT 1
#define IMPERIVM_DRIVE_CLIP_AT_DEFAULT 0.9

// dynamic 0.8 to 1
// clip fac -0.3 to 30

void init_imperivm_drive(effect_t *effect);
void imperivm_drive(effect_t *data, float *buffer, uint32_t size);


#endif /* EFFECTS_IMPERIVM_DRIVE_H_ */
