/*
 * IMPERIVM
 *
 *  Created on: Jul 17, 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_PAST_H_
#define EFFECTS_PAST_H_


#include "stdint.h"
#include "math.h"

#define MAX_PAST_TIME 0.6
#define SF 50000
#define PAST_BUFFER_SIZE (int)(SF*MAX_PAST_TIME)

extern int16_t past_buffer[];
extern int begin_of_next_block;

void init_past_buffer();
void copy_block(float *buffer, uint32_t size);
int get_begin_of_current_block();
int get_wrapped_index(int index, int size);
int get_wrapped_index_old(int index);
float int16_t_to_float(int16_t x);

#endif /* EFFECTS_PAST_H_ */
