/*
 * parameters_helpers.h
 *
 *  Created on: 8 ago 2019
 *      Author: Davide
 */

#ifndef EFFECTS_PARAMETERS_HELPERS_H_
#define EFFECTS_PARAMETERS_HELPERS_H_

#include "stdint.h"

void setContinuousLinearRealParameter(float *param, float min, float max, float step, int16_t verse);

#endif /* EFFECTS_PARAMETERS_HELPERS_H_ */
