/*
 * IMPERIVM
 *
 *  Created on: 17 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "chain.h"
#include "noise_gate.h"
#include "clip.h"
#include "booster.h"
#include "overdrive.h"
#include "imperivm_drive.h"
#include "imperivm_crusher.h"
#include "tone_eq.h"
#include "flanger.h"
#include "delay.h"
#include "parameters_helpers.h"
#include "past.h"
#include "pot_noise.h"
#include <string.h>
#include <stdio.h>

effect_t *chain[NUM_EFFECTS];

int focused_effect_index;
float focused_effect_index_incremental;
static uint8_t bypass_all;

void default_pots_management(int16_t potIndex, int16_t verse);
void default_process(effect_t *data, float *buffer, uint32_t size);
void defaultParamToString(float param, param_data_t *data, char *result);

effect_t *spawn_empty_effect()
{
	effect_t *effect = malloc(sizeof(effect_t));

	effect->process = &default_process;
	effect->pot_callback = &default_pots_management;

	for (int i = 0; i < NUM_PARAMS; i++)
	{
		effect->params[i] = 0;
		effect->params_data[i] = (param_data_t){.min = 0, .max = 0, .step = 0, .toString = defaultParamToString, .change = minmaxParamChange};
	}

	effect->selected_param = 0;

	effect->params_changed = 1;

	effect->bypass = 1;

	effect->other_data = 0;

	return effect;
}

void init_chain()
{
	bypass_all = 0;

	for (int i = 0; i < NUM_EFFECTS; i++)
	{
		chain[i] = spawn_empty_effect();
	}

	init_noise_gate(chain[NOISE_GATE_INDEX]);
	init_booster(chain[BOOSTER_INDEX]);
	init_overdrive(chain[OVERDRIVE_INDEX]);
	init_imperivm_drive(chain[IMPERIVM_DRIVE_INDEX]);
	init_imperivm_crusher(chain[IMPERIVM_CRUSHER_INDEX]);
	init_tone_eq(chain[TONE_EQ_INDEX]);
	init_flanger(chain[FLANGER_INDEX]);
	init_delay(chain[DELAY_INDEX]);
	init_pot_noise(chain[POT_NOISE_INDEX]);
	init_clip(chain[CLIP_INDEX]);

	set_focused(0);
}

void apply_chain(float *buffer, uint32_t size)
{
	// The conditions here are made so that it is impossible to activate flanger with multiple delay.
	// That would be a disaster. Seriously.

	if (!bypass_all)
	{
//		if ((int)chain[DELAY_INDEX]->params[DELAY_MULTIPLE] == 0 || (int)chain[FLANGER_INDEX]->bypass == 0)
//		{
//			copy_block(buffer, size);
//		}

		for(int i = 0; i < NUM_EFFECTS; i++)
		{
			effect_t *data = chain[i];
			if(!data->bypass)
			{
				data->process(data, buffer, size);
				data->params_changed = 0;
			}
		}

//		if ((int)chain[DELAY_INDEX]->params[DELAY_MULTIPLE] == 1 && (int)chain[FLANGER_INDEX]->bypass == 1)
//		{
//			copy_block(buffer, size);
//		}
	}

}

void set_focused(int focused_index)
{
	focused_effect_index = focused_index;
	focused_effect_index_incremental = focused_index;

}

static float wrap_float(float x, float min, float max)
{
	if (x >= max)
	{
		x = min+0.001;
	}

	if (x <= min)
	{
		x = max-0.001;
	}

	return x;
}

void inc_focused_index(float amt)
{
	focused_effect_index_incremental = (focused_effect_index_incremental + amt);

	focused_effect_index_incremental = wrap_float(focused_effect_index_incremental, 0, NUM_EFFECTS);

	if ((int)focused_effect_index_incremental != focused_effect_index)
	{
		focused_effect_index = (int)focused_effect_index_incremental;
		pot_noise_enable(chain[POT_NOISE_INDEX],1);
	}
}

void default_pots_management(int16_t potIndex, int16_t verse)
{
	effect_t *data = chain[focused_effect_index];

	if (potIndex < NUM_PARAMS)
	{
		param_data_t *pd = &(data->params_data[potIndex]);
		setContinuousLinearRealParameter(&(data->params[potIndex]), pd->min, pd->max, pd->step, verse);
	}
}

void chain_pots_pressed_management(int16_t potIndex)
{
	effect_t *data = chain[focused_effect_index];

	if (potIndex == BYPASS_POT_INDEX)
	{
		data->bypass = !data->bypass;
	}
}

void chain_pots_management(int16_t potIndex, int16_t verse)
{
	chain[focused_effect_index]->pot_callback(potIndex, verse);
}

void defaultParamToString(float param, param_data_t *data, char *result)
{
	float min = data->min;
	float max = data->max;
	float norm_val = (param-min)/(max-min)*99 + 1;
	sprintf(result, "%d", (int)norm_val);
}

void keepTheSameParamToString(float param, param_data_t *data, char *result)
{
	sprintf(result, "%d", (int)param);
}

void multiplyParamToString(float param, param_data_t *data, char *result)
{
	sprintf(result, "%d", (int)(param*data->mul_vis));
}

void onoffParamToString(float param, param_data_t *data, char *result)
{
	if ((int)param == 0)
	{
		sprintf(result, "%s", "OFF");
	}
	else
	{
		sprintf(result, "%s", "ON");
	}
}

void minmaxParamChange(float *param, float verse, struct _param_data_t *data)
{
	setContinuousLinearRealParameter(param, data->min, data->max, data->step, verse);
}

void circularParamChange(float *param, float verse, struct _param_data_t *data)
{
	float step = data->step;
	float max = data->max;
	float min = data->min;

	*param += step * verse;

	if (*param < min)
	{
		*param = max;
	}

	if (*param > max)
	{
		*param = min;
	}
}

void set_effect_base_param(effect_t *effect, int param_index, float def, float min, float max, float step, const char *name)
{
	effect->params[param_index] = def;
	effect->params_data[param_index].min = min;
	effect->params_data[param_index].max = max;
	effect->params_data[param_index].step = step;
	strcpy(effect->params_data[param_index].name,name);
}

void chain_pots_management2(int16_t potIndex, int16_t verse)
{
	effect_t *data = chain[focused_effect_index];

	if (potIndex == CHANGE_EFFECT_POT_INDEX)
	{
		inc_focused_index(FOCUSED_EFFECT_CHANGE_AMT * verse);
		//pot_noise_enable(chain[POT_NOISE_INDEX]);
	}

	if (potIndex == CHANGE_PARAM_POT_INDEX)
	{
		float old_val = data->selected_param;
		data->selected_param += FOCUSED_PARAM_CHANGE_AMT * verse;

		int maxParamIndex = 0;

		// Finding the maximum used effect
		while(data->params_data[maxParamIndex].name[0] != 0)
		{
			maxParamIndex++;
		}

		maxParamIndex = maxParamIndex > VISUALIZED_PARAMS ? VISUALIZED_PARAMS : maxParamIndex;

		data->selected_param = wrap_float(data->selected_param, 0, maxParamIndex);

		if ((int)old_val != (int)data->selected_param)
		{
			pot_noise_enable(chain[POT_NOISE_INDEX],0.5);
		}
	}

	if (potIndex == CHANGE_VALUE_POT_INDEX)
	{
		int paramIndex = (int)data->selected_param;
		param_data_t *pd = &(data->params_data[paramIndex]);

		pd->change(&(data->params[paramIndex]),verse,pd);

		data->params_changed = 1;
	}
}

void chain_pots_pressed_management2(int16_t potIndex)
{
	effect_t *data = chain[focused_effect_index];

	if (potIndex == BYPASS_POT_INDEX)
	{
		data->bypass = !data->bypass;
	}

	if (potIndex == BYPASS_ALL_POT_INDEX)
	{
		toggle_bypass_all();
	}
}

int get_bypass_all()
{
	return bypass_all;
}


int toggle_bypass_all()
{
	bypass_all = !bypass_all;
}

void default_process(effect_t *data, float *buffer, uint32_t size)
{
	asm("NOP");
}
