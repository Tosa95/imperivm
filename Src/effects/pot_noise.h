/*
 * pot_noise.h
 *
 *  Created on: 13 ago 2019
 *      Author: Davide
 */

#ifndef EFFECTS_POT_NOISE_H_
#define EFFECTS_POT_NOISE_H_

#include "stdint.h"
#include "chain.h"

#define POT_NOISE_GAIN 0

#define POT_NOISE_GAIN_DEFAULT 0.5

void init_pot_noise(effect_t *effect);
void pot_noise(effect_t *data, float *buffer, uint32_t size);
void pot_noise_enable(effect_t *data, float force);

#endif /* EFFECTS_POT_NOISE_H_ */
