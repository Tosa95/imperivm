/*
 * IMPERIVM
 *
 *  Created on: 17 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */


float trif (float t, float period)
{
	float half_period = period/2;
	if (t < half_period)
	{
		return t/half_period;
	}
	else
	{
		return 2 - t/half_period;
	}

}

