/*
 * IMPERIVM
 *
 *  Created on: 16 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include <stdlib.h>
#include "clip.h"
#include "math.h"
#include "string.h"

typedef struct {
	float clip_correct_fac;
} clip_data;

void init_clip(effect_t *effect)
{
	strcpy(effect->name, "Clip");

	effect->process = &clip;

	set_effect_base_param(effect, CLIP_CUT_AT, CLIP_CUT_AT_DEFAULT, 0.9, 1, 0.01, "CA");
	effect->params_data[CLIP_CUT_AT].mul_vis = 100;
	effect->params_data[CLIP_CUT_AT].toString = multiplyParamToString;
	set_effect_base_param(effect, CLIP_MAX, CLIP_MAX_DEFAULT, 0.9, 1, 0.01, "MX");
	effect->params_data[CLIP_MAX].mul_vis = 100;
	effect->params_data[CLIP_MAX].toString = multiplyParamToString;
	set_effect_base_param(effect, CLIP_BRUTALITY, CLIP_BRUTALITY_DEFAULT, 0.1, 3, 0.025, "B");

	effect->bypass = CLIP_BYPASS;

	effect->other_data = malloc(sizeof(clip_data));

	clip_data *cd = (clip_data *)effect->other_data;

	cd->clip_correct_fac = 1;
}

void clip_single_sample(float clip_at, float *sample)
{
	if(*sample >= clip_at)
	{
		*sample = clip_at;
	}
	if(*sample <= -clip_at)
	{
		*sample = -clip_at;
	}
}

void parametric_clip_single_sample(float clip_at, float clip_factor, float brutality, float *sample)
{
	if (*sample >= clip_at || *sample <= -clip_at)
	{
		const float fac_coeff =  (atanf((fabs(*sample)-clip_at)*brutality)/(M_PI/2));

		clip_factor = (1-fac_coeff) + fac_coeff*clip_factor;

		if(*sample >= clip_at)
		{
			*sample = clip_at + clip_factor * (*sample - clip_at);
		}
		else if(*sample <= -clip_at)
		{
			*sample = - clip_at - clip_factor * (- *sample - clip_at);
		}
	}

}

float parametric_clip_get_correction_factor(float clip_at, float clip_factor, float brutality)
{

    if (clip_factor > 0)
    {
    	return 1;
    }


    float lr = PARAMETRIC_CLIP_MAX_ESTIMATION_LR / (1+(fabs(clip_factor)*brutality)*PARAMETRIC_CLIP_MAX_ESTIMATION_LR_CORRECTION_FAC);

    float x = clip_at;
    float x1 = 100;
    float der_coeff = PARAMETRIC_CLIP_MAX_ESTIMATION_DERIVATIVE_STEP;
    int i = 0;

    while (fabs(x1)>PARAMETRIC_CLIP_MAX_ESTIMATION_DERIVATIVE_TH && i < PARAMETRIC_CLIP_MAX_ESTIMATION_MAX_IT)
    {
    	float xm = x-der_coeff/2.0;
    	parametric_clip_single_sample(clip_at,clip_factor,brutality,&xm);

    	float xp = x+der_coeff/2.0;
    	parametric_clip_single_sample(clip_at,clip_factor,brutality,&xp);

    	x1 = (xp-xm)/der_coeff;
		x += x1*lr;
		i += 1;
    }

    parametric_clip_single_sample(clip_at,clip_factor,brutality,&x);

    return x*PARAMETRIC_CLIP_MAX_ESTIMATION_MAX_CORRECTION_FACTOR;
}

void soft_clip_single_sample(float clip_at, float *sample)
{
	const float CUT_AT = 0.9f;
	const float BETA = 1.0f;
	const float F = 15.615827723665399f;
	const float ALPHA = -0.7076476846815876f;
	const float GAMMA = 2.031087937278114f;
	const float ETA = -0.5804997396513766f;

	float x = *sample;

	float xabs = fabs(x);
	float xsign = x/xabs;

	float x_sq;

	if (xabs < CUT_AT)
	{
		x_sq = x;
	}
	else
	{
		 x_sq = 1/(ETA-powf(F*(xabs+ALPHA),GAMMA)) + BETA;
		 x_sq *= xsign;
	}

	*sample = x_sq;
}

void softer_clip_single_sample(float clip_at, float *sample)
{
	const float F = 27.971628116384004;
	const float ALPHA = 0.5753268857164754;
	const float GAMMA = 0.6697060774651749;
	const float ETA = 7.878670212055327;
	const float CUT_AT = 0.5940835045815123;
	const float BETA = 1;

	float x = *sample;

	float xabs = fabs(x);
	float xsign = x/xabs;

	float x_sq;

	if (xabs < CUT_AT)
	{
		x_sq = x;
	}
	else
	{
		 x_sq = 1/(ETA-powf(F*(xabs+ALPHA),GAMMA)) + BETA;
		 x_sq *= xsign;
	}

	*sample = x_sq;
}

void clip(effect_t *data, float *buffer, uint32_t size)
{
	float clip_at = data->params[CLIP_CUT_AT];
	float max = data->params[CLIP_MAX];
	float brutality = data->params[CLIP_BRUTALITY];

	clip_data *cd = (clip_data *)data->other_data;

	if (data->params_changed)
	{
		cd->clip_correct_fac = parametric_clip_get_correction_factor(clip_at, 0, brutality);
	}

	for(int i = 0; i < size; i++)
	{
		parametric_clip_single_sample(clip_at, 0, brutality, &(buffer[i]));
		buffer[i] /= cd->clip_correct_fac;
		buffer[i] *= max;
	}
}


