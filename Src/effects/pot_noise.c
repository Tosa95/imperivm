/*
 * pot_noise.c
 *
 *  Created on: 13 ago 2019
 *      Author: Davide
 */

#include "pot_noise.h"
#include "signals.h"
#include "clip.h"
#include <string.h>

typedef struct{
	float do_noise;
}pot_noise_data;

void init_pot_noise(effect_t *effect)
{
	strcpy(effect->name, "POT Noise");
	effect->process = &pot_noise;

	set_effect_base_param(effect, POT_NOISE_GAIN, POT_NOISE_GAIN_DEFAULT, 0.05, 1, 0.05, "G");

	effect->bypass = POT_NOISE_BYPASS;

	effect->other_data = malloc(sizeof(pot_noise_data));

	pot_noise_data *pnd = (pot_noise_data*)effect->other_data;
	pnd->do_noise = 0;

}

void pot_noise(effect_t *data, float *buffer, uint32_t size)
{
	float gain = data->params[POT_NOISE_GAIN];
	pot_noise_data *pnd = (pot_noise_data*)data->other_data;

	if(pnd->do_noise)
	{
		float force = pnd->do_noise;
		for(int i = 0; i < size; i++)
		{
			buffer[i] += i<size/2?-gain*force:gain*force;
			soft_clip_single_sample(0.98, &buffer[i]);
		}
		pnd->do_noise = 0;
	}
}

void pot_noise_enable(effect_t *data, float force)
{
	pot_noise_data *pnd = (pot_noise_data*)data->other_data;

	pnd->do_noise = force;
}

