/*
 * IMPERIVM
 *
 *  Created on: 17 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_CHAIN_H_
#define EFFECTS_CHAIN_H_

#include <stdlib.h>
#include "stdint.h"
#include "../tasks/pot.h"
#include "effect_t.h"

#define NUM_PARAMS 6
#define VISUALIZED_PARAMS 5
#define NUM_EFFECTS 10

#define NOISE_GATE_BYPASS 0
#define BOOSTER_BYPASS 1
#define OVERDRIVE_BYPASS 1
#define IMPERIVM_DRIVE_BYPASS 0
#define IMPERIVM_CRUSHER_BYPASS 1
#define TONE_EQ_BYPASS 1
#define FLANGER_BYPASS 1
#define DELAY_BYPASS 1

#define POT_NOISE_BYPASS 0
#define CLIP_BYPASS 0

#define NOISE_GATE_INDEX 0
#define BOOSTER_INDEX 1
#define OVERDRIVE_INDEX 2
#define IMPERIVM_DRIVE_INDEX 3
#define IMPERIVM_CRUSHER_INDEX 4
#define TONE_EQ_INDEX 5
#define FLANGER_INDEX 6
#define DELAY_INDEX 7

#define POT_NOISE_INDEX 8
#define CLIP_INDEX 9

#define BYPASS_POT_INDEX 1
#define BYPASS_ALL_POT_INDEX 0

#define CHANGE_EFFECT_POT_INDEX 2
#define CHANGE_PARAM_POT_INDEX 1
#define CHANGE_VALUE_POT_INDEX 0

#define FOCUSED_EFFECT_CHANGE_AMT 1.0f/3
#define FOCUSED_PARAM_CHANGE_AMT 1.0f/3

#define NAME_MAX_SIZE 25
#define PARAM_NAME_MAX_SIZE 5

extern int focused_effect_index;

typedef struct _param_data_t {

	// Minimum possible value
	float min;

	// Maximum possible value
	float max;

	// How much the parameter is changed by one step
	float step;

	// Multiplication factor used to show the parameter
	float mul_vis;

	// Name of the parameter (MAX 3 CHARACTERS!, 2 is better)
	char name[PARAM_NAME_MAX_SIZE];

	// Function that converts the parameter to a string to be visualized
	void (*toString)(float param, struct _param_data_t *data, char *result);

	// Functions that changes the parameter upon rotation
	void (*change)(float *param, float verse, struct _param_data_t *data);

}param_data_t;

struct effect_t_s
{
	int bypass;
	char name[NAME_MAX_SIZE];
	float params[NUM_PARAMS];
	int params_changed;
	param_data_t params_data[NUM_PARAMS];
	float selected_param;
	void (*process) (struct effect_t_s *data, float *buffer, uint32_t size);
	potCallback_t pot_callback;
	void *other_data;
};

void init_chain();
void apply_chain(float *buffer, uint32_t size);
void set_focused(int focused_index);
void inc_focused_index(float amt);
void set_effect_base_param(effect_t *effect, int param_index, float def, float min, float max, float step, const char *name);
void keepTheSameParamToString(float param, param_data_t *data, char *result);
void multiplyParamToString(float param, param_data_t *data, char *result);
void onoffParamToString(float param, param_data_t *data, char *result);

void minmaxParamChange(float *param, float verse, struct _param_data_t *data);
void circularParamChange(float *param, float verse, struct _param_data_t *data);


void chain_pots_management(int16_t potIndex, int16_t verse);
void chain_pots_pressed_management(int16_t potIndex);

void chain_pots_management2(int16_t potIndex, int16_t verse);
void chain_pots_pressed_management2(int16_t potIndex);

int get_bypass_all();
int toggle_bypass_all();

extern effect_t *chain[NUM_EFFECTS];

#endif /* EFFECTS_CHAIN_H_ */
