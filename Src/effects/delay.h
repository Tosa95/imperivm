/*
 * IMPERIVM
 *
 *  Created on: Jul 17, 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_DELAY_H_
#define EFFECTS_DELAY_H_


#include "stdint.h"
#include "chain.h"

#define DELAY_DELAY_AMOUNT 0
#define DELAY_MIX 1
#define DELAY_MULTIPLE 2

#define DELAY_DELAY_AMOUNT_DEFAULT 0.380
#define DELAY_MIX_DEFAULT 0.6
#define DELAY_MULTIPLE_DEFAULT 1

void init_delay(effect_t *effect);
void delay(effect_t *data, float *buffer, uint32_t size);


#endif /* EFFECTS_DELAY_H_ */
