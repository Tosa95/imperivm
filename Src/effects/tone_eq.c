/*
 * IMPERIVM
 *
 *  Created on: 26 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "main.h"
#include "math.h"
#include "tone_eq.h"

typedef struct {
	float old_sample;
} tone_eq_data;

void init_tone_eq(effect_t *effect)
{
	strcpy(effect->name, "Tone EQ");
	effect->process = &tone_eq;

	set_effect_base_param(effect, TONE_EQ_CUTOFF_FREQ, TONE_EQ_CUTOFF_FREQ_DEFAULT, 0.01, 0.5, 0.005, "Fc");

	effect->bypass = TONE_EQ_BYPASS;

	effect->other_data = malloc(sizeof(tone_eq_data));

	tone_eq_data *ted = (tone_eq_data *)effect->other_data;

	ted->old_sample = 0;
}

void tone_eq(effect_t *data, float *buffer, uint32_t size)
{
	float cutoff_freq = data->params[TONE_EQ_CUTOFF_FREQ];

	tone_eq_data *ted = (tone_eq_data *)data->other_data;

	float decay = powf(M_E, -2*M_PI*cutoff_freq);

	float original_energy = 0;
	float equalized_energy = 0;

	for(int i = 0; i < size; i++)
	{
		original_energy += buffer[i] * buffer[i];
		buffer[i] = (1 - decay) * buffer[i] + decay * ted->old_sample;
		equalized_energy += buffer[i] * buffer[i];

		ted->old_sample = buffer[i];
	}


	// adjust the output level
	// qua la normalizzazione fatta così non va bene, rovina il segnale: si creano dei salti fastidiosissimi, soprattutto con Fc bassa.
	// Bisogna o mediare tra nel tempo o lasciare perdere

//	float level = sqrtf(original_energy)/sqrtf(equalized_energy);
//
//	for(int i = 0; i < size; i++)
//	{
//		buffer[i] *= level;
//	}
}


