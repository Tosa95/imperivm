/*
 * IMPERIVM
 *
 *  Created on: Jul 17, 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "main.h"
#include "delay.h"
#include "past.h"
#include <string.h>

void init_delay(effect_t *effect)
{
	strcpy(effect->name, "Delay");

	effect->process = &delay;

	set_effect_base_param(effect, DELAY_DELAY_AMOUNT, DELAY_DELAY_AMOUNT_DEFAULT, 0, MAX_PAST_TIME - 0.01, 0.005, "AM");
	effect->params_data[DELAY_DELAY_AMOUNT].mul_vis = 1000;
	effect->params_data[DELAY_DELAY_AMOUNT].toString = multiplyParamToString;
	set_effect_base_param(effect, DELAY_MIX, DELAY_MIX_DEFAULT, 0, 0.9, 0.02, "M");
	set_effect_base_param(effect, DELAY_MULTIPLE, DELAY_MULTIPLE_DEFAULT, 0, 1.99, 1.0/3.0, "MU");
	effect->params_data[DELAY_MULTIPLE].toString = onoffParamToString;
	effect->params_data[DELAY_MULTIPLE].change = circularParamChange;

	effect->bypass = DELAY_BYPASS;

	effect->other_data = 0;
}

void delay(effect_t *data, float *buffer, uint32_t size)
{
	float delay_amount = data->params[DELAY_DELAY_AMOUNT];
	float mix = data->params[DELAY_MIX];
	int delay_multiple = data->params[DELAY_MULTIPLE];

	int delay_samples = (int)(delay_amount * SF);

	if(delay_multiple == 0)
	{
		copy_block(buffer, size);
	}

	int j = get_begin_of_current_block();

	for(int i = 0; i < size; i++)
	{
		buffer[i] = buffer[i] + mix * int16_t_to_float(past_buffer[get_wrapped_index_old(j - delay_samples)]);
		j++;
	}

	if(delay_multiple == 1)
	{
		copy_block(buffer, size);
	}
}





