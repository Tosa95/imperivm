/*
 * IMPERIVM
 *
 *  Created on: 27 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_LFO_H_
#define EFFECTS_LFO_H_

#define LFO_WAVE_SIN 0
#define LFO_WAVE_SIN4 1
#define LFO_WAVE_TRI 2

#define LFO_WAVES_NUM 3

#include "chain.h"

float lfo(float min_value, float max_value, float freq, float t, int wave_shape);
void lfoWaveToString(float param, param_data_t *data, char *result);

#endif /* EFFECTS_LFO_H_ */
