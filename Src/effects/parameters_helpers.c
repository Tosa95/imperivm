/*
 * parameters_helpers.c
 *
 *  Created on: 8 ago 2019
 *      Author: Davide
 */

#include "parameters_helpers.h"

void setContinuousLinearRealParameter(float *param, float min, float max, float step, int16_t verse)
{
	*param += step * verse;

	if (*param < min)
	{
		*param = min;
	}

	if (*param > max)
	{
		*param = max;
	}
}
