/*
 * IMPERIVM
 *
 *  Created on: 26 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef EFFECTS_TONE_EQ_H_
#define EFFECTS_TONE_EQ_H_


#include "stdint.h"
#include "chain.h"

#define TONE_EQ_CUTOFF_FREQ 0

#define TONE_EQ_CUTOFF_FREQ_DEFAULT 0.05

// TONE_EQ_CUTOFF_FREQ_DEFAULT from 0 to 0.5
//	- the real cutoff frequency in Hz will be:
//		SAMPLING_FREQUENCY * TONE_EQ_CUTOFF_FREQ
//	  for example: 50000 * 0.05 = 2500 Hz

void init_tone_eq(effect_t *effect);
void tone_eq(effect_t *data, float *buffer, uint32_t size);


#endif /* EFFECTS_TONE_EQ_H_ */
