/*
 * IMPERIVM
 *
 *  Created on: 29 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "overdrive.h"
#include "math.h"
#include "main.h"
#include "global_constants.h"

#define LEVEL_KEEP_BUFFERS 1 // for the moving average
#define GAIN_DELTA_AMOUNT 0.1

typedef struct {
	float old_level;
} overdrive_data;

void init_overdrive(effect_t *effect)
{
	strcpy(effect->name, "Overdrive");
	effect->process = &overdrive;

	set_effect_base_param(effect, OVERDRIVE_GAIN, OVERDRIVE_GAIN_DEFAULT, 1, 30, 0.2, "G");
	effect->params_data[OVERDRIVE_GAIN].toString = keepTheSameParamToString;
	set_effect_base_param(effect, OVERDRIVE_CLIP_AT, OVERDRIVE_CLIP_AT_DEFAULT, 0.9, 1, 0.01, "CA");

	effect->bypass = OVERDRIVE_BYPASS;

	effect->other_data = malloc(sizeof(overdrive_data));

	overdrive_data *odd = (overdrive_data *)effect->other_data;
	odd->old_level = 1;

	return effect;
}

void overdrive(effect_t *data, float *buffer, uint32_t size)
{
	float gain = data->params[OVERDRIVE_GAIN];
	float clip_at = data->params[OVERDRIVE_CLIP_AT];

	overdrive_data *odd = (overdrive_data *)data->other_data;

	float original_energy = 0;
	float clipped_energy = 0;

	for(int i = 0; i < size; i++)
	{

		original_energy += buffer[i] * buffer[i];

		// perform the boost using a point-wise function

		float x = buffer[i];

		if(fabs(x) <= 1/(3*gain))
		{
			buffer[i] = clip_at * 2*gain*x;
		}
		else if(fabs(x) <= 2/(3*gain))
		{
			buffer[i] = clip_at * (x/fabs(x))*(3 - (powf((2 - 3*gain*fabs(x)), 2)))/3;
		}
		else
		{
			buffer[i] = clip_at * (x/fabs(x));
		}

		clipped_energy += buffer[i] * buffer[i];

	}


	// adjust the output level

	float level = sqrtf(original_energy)/sqrtf(clipped_energy);

	level = (level + LEVEL_KEEP_BUFFERS * odd->old_level) / (1 + LEVEL_KEEP_BUFFERS);

	if (original_energy/size > SILENCE_AVG_ENERGY)
	{
		odd->old_level = level;
	}
	for(int i = 0; i < size; i++)
	{
		buffer[i] *= level;
	}


}



