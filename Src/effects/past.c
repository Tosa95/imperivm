/*
 * IMPERIVM
 *
 *  Created on: Jul 17, 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */
#include "past.h"
#include "math.h"
#include "../buffers.h"

static float _2p16;

int16_t past_buffer[PAST_BUFFER_SIZE];
int begin_of_next_block = 0;

void init_past_buffer()
{
	_2p16 = powf(2,16);
	for(int i = 0; i < PAST_BUFFER_SIZE; i++)
	{
		past_buffer[i] = 0;
	}
}

int16_t float_to_int16_t(float x)
{
	soft_clip_single_sample(0.98, &x);
	return (int)(x/2*_2p16);
}

float int16_t_to_float(int16_t x)
{
	return (((float)x)/_2p16)*2;
}

void copy_block(float *buffer, uint32_t size)
{
	for(int i = 0; i < size; i++)
	{
		past_buffer[begin_of_next_block] = float_to_int16_t(buffer[i]);
		begin_of_next_block ++;
		if (begin_of_next_block >= PAST_BUFFER_SIZE)
		{
			begin_of_next_block = 0;
		}
	}
}

int get_begin_of_current_block()
{
	int res = begin_of_next_block - HALF_BUFFER_SIZE;
	if(res < 0)
	{
		res = res + PAST_BUFFER_SIZE;
	}
	return res;
}

int get_wrapped_index(int index, int size)
{
	if(index >= size)
	{
		return index - size;
	}
	if(index < 0)
	{
		return size + index;
	}
}

int get_wrapped_index_old(int index)
{
	if(index >= PAST_BUFFER_SIZE)
	{
		return index - PAST_BUFFER_SIZE;
	}
	if(index < 0)
	{
		return PAST_BUFFER_SIZE + index;
	}
}




