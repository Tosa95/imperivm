/*
 * IMPERIVM
 *
 *  Created on: 18 lug 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include <stdlib.h>
#include "noise_gate.h"
#include "math.h"

typedef struct{
	int n_samples_in_threshold;
}noise_gate_data;

void init_noise_gate(effect_t *effect)
{
	strcpy(effect->name, "Noise Gate");
	effect->process = &noise_gate;

	set_effect_base_param(effect, NOISE_GATE_THRESHOLD, NOISE_GATE_THRESHOLD_DEFAULT, 0.001, 0.1, 0.0002, "T");
	set_effect_base_param(effect, NOISE_GATE_ACTIVATION_FACTOR, NOISE_GATE_ACTIVATION_FACTOR_DEFAULT, 10000, 70000, 500, "AF");
	set_effect_base_param(effect, NOISE_GATE_DECAY_EXP, NOISE_GATE_DECAY_EXP_DEFAULT, 0.1, 5, 0.1, "DE");

	effect->bypass = NOISE_GATE_BYPASS;

	effect->other_data = (noise_gate_data*)malloc(sizeof(noise_gate_data));

	noise_gate_data *ngd = (noise_gate_data*)effect->other_data;
	ngd->n_samples_in_threshold = NOISE_GATE_ACTIVATION_FACTOR_DEFAULT;
}

void noise_gate(effect_t *data, float *buffer, uint32_t size)
{
	float threshold = data->params[NOISE_GATE_THRESHOLD];

	noise_gate_data *ngd = (noise_gate_data*)data->other_data;

	float activation_factor = data->params[NOISE_GATE_ACTIVATION_FACTOR];
	float decay_exp = data->params[NOISE_GATE_DECAY_EXP];

	for(int i = 0; i < size; i++)
	{
		if(fabs(buffer[i]) < threshold)
		{
			ngd->n_samples_in_threshold++;
		}
		else
		{
			ngd->n_samples_in_threshold = 0;
		}

		if(ngd->n_samples_in_threshold >= activation_factor)
		{
			buffer[i] /= powf(ngd->n_samples_in_threshold/activation_factor,decay_exp);
		}
	}
}


