/*
 * buffers.h
 *
 *  Created on: Jul 1, 2019
 *      Author: mrizz
 */

#ifndef BUFFERS_H_
#define BUFFERS_H_
#include "stdint.h"

// Note: always a multiple of 2
#define BUFFER_SIZE 512
#define HALF_BUFFER_SIZE BUFFER_SIZE/2

extern uint32_t ADC_DMA_BUFFER[];
extern float PROCESSING_BUFFER[];
extern uint32_t DAC_DMA_BUFFER[];

extern int WHICH_HALF;

void ADC_buffer_to_processing_buffer();
void processing_buffer_to_DAC_buffer();

#endif /* BUFFERS_H_ */
