#include "identity_test.h"
#include "../peripherals.h"
#include "stm32f4xx_hal.h"

void identity() {
	HAL_StatusTypeDef res = HAL_ADC_PollForConversion(&hadc1, 1);

	if (res == HAL_OK) {
		uint32_t ADCValue = HAL_ADC_GetValue(&hadc1);
		HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, ADCValue);
	}
}

