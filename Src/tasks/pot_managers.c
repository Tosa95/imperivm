/*
 * pot_managers.c
 *
 *  Created on: 12 ago 2019
 *      Author: Davide
 */

#include "pot_managers.h"
#include "../effects/chain.h"
#include "../tasks/gui.h"
#include "pot.h"

static int currManager = POT_MANAGER_CHAIN;

static const potManager_t managers[] = {
		{
				.potCb = chain_pots_management2,
				.potPressedCb = chain_pots_pressed_management2
		},
		{
				.potCb = gui_pots_management,
				.potPressedCb = gui_pots_pressed_management
		}
};

void setPotManager(int managerIndex)
{
	potCallback = managers[managerIndex].potCb;
	potPressedCallback =  managers[managerIndex].potPressedCb;
	currManager = managerIndex;
}

void nextPotManager()
{
	currManager++;
	currManager %= POT_MANAGERS_NUM;
	setPotManager(currManager);
}

int getCurrPotManager()
{
	return currManager;
}
