/*
 * IMPERIVM
 *
 *  Created on: 6 ago 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef TASKS_TEST_FLASH_H_
#define TASKS_TEST_FLASH_H_


void testFlashTask(void *argument);


#endif /* TASKS_TEST_FLASH_H_ */
