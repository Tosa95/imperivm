#include "cmsis_os.h"
#include "handlers.h"
#include "../buffers.h"
#include "../calibration.h"
#include "main.h"
#include "processing.h"
#include "../effects/chain.h"
#include "../effects/past.h"


osThreadId_t processingHandle;
osSemaphoreId_t processingSemaphore;

void processingTask(void *argument)
{

	while (1)
	{
		osStatus_t status = osSemaphoreAcquire(processingSemaphore, osWaitForever);
		ADC_buffer_to_processing_buffer();

		if(HAL_GPIO_ReadPin(GPIOC, B1_Pin) == GPIO_PIN_SET)
		{
			float *processing_buffer = WHICH_HALF == 0 ? PROCESSING_BUFFER : PROCESSING_BUFFER + HALF_BUFFER_SIZE;

			float avg = multiple_avg(processing_buffer, HALF_BUFFER_SIZE);

			apply_chain(processing_buffer, HALF_BUFFER_SIZE);
		}

		WHICH_HALF = WHICH_HALF == 0 ? 1 : 0;
		processing_buffer_to_DAC_buffer();
	}
}


