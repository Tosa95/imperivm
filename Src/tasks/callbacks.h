/*
 * IMPERIVM
 *
 *  Created on: 7 ago 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef TASKS_CALLBACKS_H_
#define TASKS_CALLBACKS_H_

#include "stdint.h"

typedef void (* potCallback_t)(int16_t potIndex, int16_t verse);
typedef void (* potPressedCallback_t)(int16_t potIndex);


#endif /* TASKS_CALLBACKS_H_ */
