/*
 * IMPERIVM
 *
 *  Created on: Aug 6, 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "cmsis_os.h"
#include "handlers.h"
#include "main.h"
#include "pot.h"
#include "pot_managers.h"
#include "../effects/chain.h"

#define DELAY_AMOUNT_TICKS 10
#define POTS_NUM 3
#define WAIT_BEFORE_APPLY_ACTION_MS_POT 2
#define WAIT_BEFORE_APPLY_ACTION_MS_SW 50


osThreadId_t potHandle;

typedef struct {
	GPIO_TypeDef *portCHA;
	GPIO_TypeDef *portCHB;
	GPIO_TypeDef *portSwitch;

	uint16_t pinCHA;
	uint16_t pinCHB;
	uint16_t pinSwitch;

	uint8_t CHAState;
	uint8_t CHBState;
	uint8_t switchState;

	uint8_t hasBeenRotated;
	uint32_t tickRotated;

	uint8_t hasBeenPressed;
	uint32_t tickPressed;

} potData_t;

		potData_t pots[POTS_NUM] = {
		{
				.portCHA = GPIOB,
				.portCHB = GPIOB,
				.portSwitch = GPIOA,

				.pinCHA = POT1_CHA_Pin,
				.pinCHB = POT1_CHB_Pin,
				.pinSwitch = POT1_SW_Pin,

				.CHAState = GPIO_PIN_SET,
				.CHBState = GPIO_PIN_SET,
				.switchState = GPIO_PIN_SET,

				.hasBeenRotated = 0,
				.tickRotated = 0,
				.hasBeenPressed = 0,
				.tickPressed = 0
		},
		{
				.portCHA = GPIOC,
				.portCHB = GPIOB,
				.portSwitch = GPIOB,

				.pinCHA = POT2_CHA_Pin,
				.pinCHB = POT2_CHB_Pin,
				.pinSwitch = POT2_SW_Pin,

				.CHAState = GPIO_PIN_SET,
				.CHBState = GPIO_PIN_SET,
				.switchState = GPIO_PIN_SET,

				.hasBeenRotated = 0,
				.tickRotated = 0,
				.hasBeenPressed = 0,
				.tickPressed = 0
		},
		{
				.portCHA = GPIOA,
				.portCHB = GPIOB,
				.portSwitch = GPIOB,

				.pinCHA = POT3_CHA_Pin,
				.pinCHB = POT3_CHB_Pin,
				.pinSwitch = 0,

				.CHAState = GPIO_PIN_SET,
				.CHBState = GPIO_PIN_SET,
				.switchState = GPIO_PIN_SET,

				.hasBeenRotated = 0,
				.tickRotated = 0,
				.hasBeenPressed = 0,
				.tickPressed = 0
		}
};

potCallback_t potCallback;
potPressedCallback_t potPressedCallback;

void defaultPotCallback(int16_t potIndex, int16_t verse)
{
	if(verse == POT_VERSE_INCREASE)
	{
		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
	}
}

void potTask(void *argument)
{


	while (1)
	{
		for(int i = 0; i < POTS_NUM; i++)
		{

			potData_t *potData = &pots[i];


			if(potData->hasBeenRotated)
			{
				if(HAL_GetTick() - potData->tickRotated > WAIT_BEFORE_APPLY_ACTION_MS_POT)
				{
					int pi = i;

#ifdef OVERRIDE_POT_INDEX
					pi = OVERRIDE_POT_INDEX;
#endif

					if(potData->CHAState == potData->CHBState)
					{
						if(potCallback != 0)
						{
							potCallback(pi, POT_VERSE_INCREASE);
						}
						//HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
					}
					else
					{
						if(potCallback != 0)
						{
							potCallback(pi, POT_VERSE_DECREASE);
						}

						//HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
					}

					potData->hasBeenRotated = 0;
				}

			}

			if(potData->hasBeenPressed && ((HAL_GetTick() - potData->tickPressed) > WAIT_BEFORE_APPLY_ACTION_MS_SW))
			{
				if (i == POT_MANAGER_SWITCH_INDEX)
				{
					nextPotManager();
				}
				else if (potPressedCallback != 0)
				{
					potPressedCallback(i);
				}

				potData->hasBeenPressed = 0;
			}

		}

		osDelay(DELAY_AMOUNT_TICKS);

	}

}


void managePotsInterrupts(uint16_t pin)
{


	for(int i = 0; i < POTS_NUM; i++)
	{
		potData_t *potData = &pots[i];

		if(pin == potData->pinCHA)
		{
			uint8_t stateA = HAL_GPIO_ReadPin(potData->portCHA, potData->pinCHA);
			uint8_t stateB = HAL_GPIO_ReadPin(potData->portCHB, potData->pinCHB);

			potData->CHAState = stateA;
			potData->CHBState = stateB;

			potData->hasBeenRotated = 1;
			potData->tickRotated = HAL_GetTick();

			break;
		}

		if(pin == potData->pinSwitch && HAL_GPIO_ReadPin(potData->portSwitch, potData->pinSwitch) == GPIO_PIN_SET)
		{
			potData->hasBeenPressed = 1;
			potData->tickPressed = HAL_GetTick();

			break;
		}
	}
}



