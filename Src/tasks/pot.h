/*
 * IMPERIVM
 *
 *  Created on: Aug 6, 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#ifndef TASKS_POT_H_
#define TASKS_POT_H_


#include "../effects/effect_t.h"
#include "callbacks.h"

#define POT_VERSE_INCREASE 1
#define POT_VERSE_DECREASE -1

#define POT_MANAGER_SWITCH_INDEX 99999

//#define OVERRIDE_POT_INDEX 2

void potTask(void *argument);
void managePotsInterrupts(uint16_t pin);

extern potCallback_t potCallback;
extern potPressedCallback_t potPressedCallback;

#endif /* TASKS_POT_H_ */
