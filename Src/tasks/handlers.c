#include "handlers.h"
#include "processing.h"
#include "pot.h"
#include "gui.h"
#include "test_flash.h"

#define STACK_SIZE 1024

osSemaphoreAttr_t processingSemaphoreAttributes = {
		.name = "processingSemaphore"
};

const osThreadAttr_t processingTaskAttributes = {
		.name = "processingTask",
		.priority = (osPriority_t) osPriorityRealtime,
		.stack_size = STACK_SIZE
};

const osThreadAttr_t potTaskAttributes = {
		.name = "potTask",
		.priority = (osPriority_t) osPriorityLow,
		.stack_size = STACK_SIZE
};

const osThreadAttr_t guiTaskAttributes = {
		.name = "guiTask",
		.priority = (osPriority_t) osPriorityLow,
		.stack_size = STACK_SIZE*4
};

const osThreadAttr_t testFlashTaskAttributes = {
		.name = "testFlashTask",
		.priority = (osPriority_t) osPriorityLow,
		.stack_size = STACK_SIZE
};

void initializeTasks()
{
	// Processing task
	processingSemaphore = osSemaphoreNew((uint32_t) 100, (uint32_t) 0,
			&processingSemaphoreAttributes);
	processingHandle = osThreadNew(processingTask, NULL,
			&processingTaskAttributes);

	// Pot task
	potHandle = osThreadNew(potTask, NULL, &potTaskAttributes);

	// GUI task
	guiHandle = osThreadNew(guiTask, NULL, &guiTaskAttributes);

	// TEST flash task
	// testFlashHandle = osThreadNew(testFlashTask, NULL, &testFlashTaskAttributes);
}

