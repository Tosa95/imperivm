#ifndef TASKS_HANDLERS_H_
#define TASKS_HANDLERS_H_


#include "cmsis_os.h"

extern osThreadId_t processingHandle;
extern osThreadId_t potHandle;
extern osThreadId_t guiHandle;
extern osThreadId_t testFlashHandle; // For test only

extern osSemaphoreId_t processingSemaphore;


void initializeTasks();

#endif /* TASKS_HANDLERS_H_ */
