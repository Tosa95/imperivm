/*
 * pot_managers.h
 *
 *  Created on: 12 ago 2019
 *      Author: Davide
 */

#ifndef TASKS_POT_MANAGERS_H_
#define TASKS_POT_MANAGERS_H_

#include "callbacks.h"

typedef struct{
	potCallback_t potCb;
	potPressedCallback_t potPressedCb;
}potManager_t;

#define POT_MANAGER_CHAIN 0
#define POT_MANAGER_GUI	1

#define POT_MANAGERS_NUM 2

void setPotManager(int managerIndex);
void nextPotManager();
int getCurrPotManager();

#endif /* TASKS_POT_MANAGERS_H_ */
