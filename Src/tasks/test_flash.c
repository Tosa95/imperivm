/*
 * IMPERIVM
 *
 *  Created on: 6 ago 2019
 *      Authors: Davide Tosatto, Michele Rizzo
 */

#include "cmsis_os.h"
#include "handlers.h"
#include "main.h"
#include "test_flash.h"

#define DELAY_AMOUNT_TICKS 1000

__attribute__((__section__(".user_data"))) uint8_t flash_memory_data[64];

osThreadId_t testFlashHandle;


void write_flash(uint32_t address, uint8_t data)
{
    __disable_irq();
	HAL_FLASH_Unlock();
     __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR);
     FLASH_Erase_Sector(FLASH_SECTOR_6, VOLTAGE_RANGE_3);
     HAL_FLASH_Program(TYPEPROGRAM_WORD, address, data);
     HAL_FLASH_Lock();
     __enable_irq();
}

void testFlashTask(void *argument)
{
	uint32_t address = 0x08040000;
	uint8_t data = 120;

	write_flash(address, data);

	uint8_t read_data = flash_memory_data[0];

	osDelay(DELAY_AMOUNT_TICKS);
}


