#include "gui.h"
#include "../gui/display.h"
#include "cmsis_os.h"
#include "../gui/lvgl/lvgl.h"
#include "../effects/chain.h"
#include <stdio.h>
#include <string.h>
#include "../tasks/pot_managers.h"

#define REFRESH_EVERY_TICKS	100

osThreadId_t guiHandle;

static lv_obj_t * main_container;
static lv_obj_t * title;
static lv_obj_t * active;
static lv_obj_t * params_container;
static lv_obj_t * single_par_cont[VISUALIZED_PARAMS];
static lv_obj_t * params[VISUALIZED_PARAMS];
static lv_obj_t * params_names[VISUALIZED_PARAMS];
static lv_obj_t * leds_container;
static lv_obj_t * leds[NUM_EFFECTS];
static lv_obj_t * led_container[NUM_EFFECTS];

static lv_style_t style;
static lv_style_t param_lbl_style;
static lv_style_t all_params_cont_style;
static lv_style_t param_cont_style;
static lv_style_t param_cont_style_selected;
static lv_style_t led_style;
static lv_style_t led_style_inactive;
static lv_style_t led_cont_style;
static lv_style_t led_cont_style_selected;

void setStyle()
{
	lv_style_copy(&style, &lv_style_plain);
	lv_style_copy(&param_lbl_style, &lv_style_plain);
	lv_style_copy(&all_params_cont_style, &lv_style_plain);

	all_params_cont_style.body.padding.bottom = 0;
	all_params_cont_style.body.padding.top = 0;
	all_params_cont_style.body.padding.left = 2;
	all_params_cont_style.body.padding.right = 0;
	all_params_cont_style.body.padding.inner = 0;

	lv_style_copy(&param_cont_style, &lv_style_plain);

	param_cont_style.body.padding.bottom = 2;
	param_cont_style.body.padding.top = 3;
	param_cont_style.body.padding.left = 2;
	param_cont_style.body.padding.right = 0;
	param_cont_style.body.padding.inner = 2;

	lv_style_copy(&param_cont_style_selected, &param_cont_style);

//	param_cont_style.text.color = lv_color_make(0xff,0xff,0xff);
//	//param_cont_style.body.padding.left = 5; //For some reason it does not work...
//	param_cont_style.body.padding.inner = 0;

//	param_cont_style_selected.text.color = lv_color_make(0xff,0xff,0xff);
	param_cont_style_selected.body.border.color = lv_color_make(0x00,0x00,0x00);
	param_cont_style_selected.body.border.width = 1;
//	param_cont_style_selected.body.border.part = LV_BORDER_TOP;

	lv_style_copy(&led_style, &lv_style_plain);
	led_style.body.border.width = 1;
	led_style.body.border.color = lv_color_make(0x00,0x00,0x00);

	lv_style_copy(&led_style_inactive, &led_style);
	led_style_inactive.body.border.width = 0;

	lv_style_copy(&led_cont_style, &lv_style_plain);
	led_cont_style.body.padding.inner = 0;
	led_cont_style.body.padding.top = 1;
	led_cont_style.body.padding.bottom = 1;
	led_cont_style.body.padding.left = 1;
	led_cont_style.body.padding.right = 1;


	lv_style_copy(&led_cont_style_selected, &led_cont_style);
	led_cont_style_selected.body.border.width = 1;
	led_cont_style_selected.body.border.color = lv_color_make(0x00,0x00,0x00);


//	led_style_selected.body.border.width = 1;
//	led_style_selected.body.border.color = lv_color_make(0x00,0x00,0x00);

}

void mainInterface()
{

	lv_obj_clean(lv_scr_act());

	main_container = lv_cont_create(lv_scr_act(), NULL);
	lv_obj_set_size(main_container, 128, 64);
	lv_obj_set_pos(main_container,0,0);
	lv_cont_set_layout(main_container, LV_LAYOUT_COL_M);
	lv_cont_set_style(main_container, LV_CONT_STYLE_MAIN, &param_cont_style);

	title = lv_label_create(main_container, NULL);
	lv_label_set_text(title, "IMPERIVM");
	lv_obj_set_pos(title,0,0);
	lv_label_set_style(title, LV_LABEL_STYLE_MAIN, &style);

	active = lv_label_create(main_container, NULL);
	lv_label_set_text(active, "ACTIVE");
	lv_obj_set_pos(active,0,30);
	lv_label_set_style(active, LV_LABEL_STYLE_MAIN, &style);

	params_container = lv_cont_create(lv_scr_act(), NULL);
	lv_obj_set_size(params_container, 128, 23);
	lv_obj_set_pos(params_container, 0,64-23);
	lv_cont_set_layout(params_container, LV_LAYOUT_PRETTY);
	lv_cont_set_style(params_container, LV_CONT_STYLE_MAIN, &all_params_cont_style);

	for (int i = 0; i < VISUALIZED_PARAMS; i++)
	{

		single_par_cont[i] = lv_cont_create(params_container, NULL);
		lv_obj_set_size(single_par_cont[i], 24, 22);
		lv_cont_set_layout(single_par_cont[i], LV_LAYOUT_CENTER);
		lv_cont_set_fit2(single_par_cont[i], LV_FIT_NONE, LV_FIT_TIGHT);
		lv_cont_set_style(single_par_cont[i], LV_CONT_STYLE_MAIN, &param_cont_style);

		char txt[2];

		sprintf(txt,"P%d",i);
		params_names[i] = lv_label_create(single_par_cont[i], NULL);
		lv_label_set_text(params_names[i], txt);
		lv_label_set_style(params_names[i], LV_LABEL_STYLE_MAIN, &style);

		sprintf(txt,"P%d",i);
		params[i] = lv_label_create(single_par_cont[i], NULL);
		lv_label_set_text(params[i], txt);
		lv_label_set_style(params[i], LV_LABEL_STYLE_MAIN, &style);
	}

	leds_container = lv_cont_create(lv_scr_act(), NULL);
	lv_obj_set_size(leds_container, 128-6, 15);
	lv_obj_set_pos(leds_container, 3,25);
	lv_cont_set_layout(leds_container, LV_LAYOUT_PRETTY);
	lv_cont_set_style(leds_container, LV_CONT_STYLE_MAIN, &all_params_cont_style);

	for (int i = 0; i < NUM_EFFECTS; i++)
	{
		led_container[i] = lv_cont_create(leds_container, NULL);
		lv_cont_set_layout(led_container[i], LV_LAYOUT_PRETTY);
		lv_cont_set_fit(led_container[i], LV_FIT_TIGHT);
		lv_cont_set_style(led_container[i], LV_CONT_STYLE_MAIN, &led_cont_style);

		leds[i] = lv_led_create(led_container[i], NULL);
		lv_obj_set_size(leds[i], 4, 4);
		lv_led_set_style(leds[i], LV_LED_STYLE_MAIN, &led_style);
		lv_led_off(leds[i]);
	}
}

void guiTask(void *argument)
{
	int s = 0;

	setStyle();

	while (1)
	{
		mainInterface();

		effect_t *data = chain[focused_effect_index];

		char name[30];

		if (getCurrPotManager()==POT_MANAGER_CHAIN)
		{
			strcpy(name,data->name);
		}
		else if (getCurrPotManager()==POT_MANAGER_GUI)
		{
			sprintf(name, "<%s>", data->name);
		}

		lv_label_set_text(title, name);
		lv_label_set_text(active, get_bypass_all()?"CHAIN OFF":data->bypass?"OFF":"ON");

		for (int i = 0; i < VISUALIZED_PARAMS; i++)
		{
			char txt[50];
			data->params_data[i].toString(data->params[i],&(data->params_data[i]),txt);

			if (data->params_data[i].name[0] != 0)
			{
				lv_label_set_text(params[i], txt);
				lv_label_set_text(params_names[i], data->params_data[i].name);

				if (i == (int)data->selected_param)
				{
					lv_cont_set_style(single_par_cont[i], LV_CONT_STYLE_MAIN, &param_cont_style_selected);
				}
				else
				{
					lv_cont_set_style(single_par_cont[i], LV_CONT_STYLE_MAIN, &param_cont_style);
				}

			}
			else
			{
				lv_label_set_text(params[i], "");
				lv_label_set_text(params_names[i], "");
				lv_cont_set_style(single_par_cont[i], LV_CONT_STYLE_MAIN, &param_cont_style);
			}
		}

		for (int i = 0; i < NUM_EFFECTS; i++)
		{
			if (i == focused_effect_index)
			{
				lv_cont_set_style(led_container[i], LV_CONT_STYLE_MAIN, &led_cont_style_selected);
			}
			else
			{
				lv_cont_set_style(led_container[i], LV_CONT_STYLE_MAIN, &led_cont_style);
			}

			if (!get_bypass_all())
			{
				lv_cont_set_style(leds[i], LV_CONT_STYLE_MAIN, &led_style);
				if (chain[i]->bypass == 0)
				{
					lv_led_off(leds[i]);
				}
				else
				{
					lv_led_on(leds[i]);
				}
			}
			else
			{
				lv_cont_set_style(leds[i], LV_CONT_STYLE_MAIN, &led_style_inactive);
				lv_led_on(leds[i]);
			}

		}

	  	lv_tick_inc(REFRESH_EVERY_TICKS);
	  	lv_task_handler();
	  	refreshDisplay();

		osDelay(REFRESH_EVERY_TICKS);
	}

}

void gui_pots_management(int16_t potIndex, int16_t verse)
{
	if (potIndex == GUI_SELECT_EFFECT_POT_INDEX)
	{
		inc_focused_index(FOCUSED_EFFECT_CHANGE_AMT * verse);
	}
}

void gui_pots_pressed_management(int16_t potIndex)
{

}
