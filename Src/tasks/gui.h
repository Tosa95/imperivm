/*
 * gui.h
 *
 *  Created on: Aug 11, 2019
 *      Author: Davide
 */

#ifndef TASKS_GUI_H_
#define TASKS_GUI_H_

#include "stdint.h"

#define GUI_SELECT_EFFECT_POT_INDEX 0


void guiTask(void *argument);
void gui_pots_management(int16_t potIndex, int16_t verse);
void gui_pots_pressed_management(int16_t potIndex);

#endif /* TASKS_GUI_H_ */
