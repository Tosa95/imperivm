#ifndef CALIBRATION_H_
#define CALIBRATION_H_

// Before high impedance
//#define EMPTY_SIGNAL_AVG  0.0207397453

// After high impedance
//#define EMPTY_SIGNAL_AVG  0.321336746

#define EMPTY_SIGNAL_AVG  0.014863279

#define KEEP_AVERAGE_FACTOR 5000

float compute_avg(float *vect, int size);
float multiple_avg(float *vect, int size);

#endif /* CALIBRATION_H_ */
