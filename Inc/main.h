/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define DISP_RES_Pin GPIO_PIN_0
#define DISP_RES_GPIO_Port GPIOC
#define SIGNAL_IN_Pin GPIO_PIN_0
#define SIGNAL_IN_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define SIGNAL_OUT_Pin GPIO_PIN_4
#define SIGNAL_OUT_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define POT1_SW_Pin GPIO_PIN_6
#define POT1_SW_GPIO_Port GPIOA
#define POT1_SW_EXTI_IRQn EXTI9_5_IRQn
#define D11_Pin GPIO_PIN_7
#define D11_GPIO_Port GPIOA
#define POT2_SW_Pin GPIO_PIN_0
#define POT2_SW_GPIO_Port GPIOB
#define POT2_SW_EXTI_IRQn EXTI0_IRQn
#define POT2_CHA_Pin GPIO_PIN_7
#define POT2_CHA_GPIO_Port GPIOC
#define POT2_CHA_EXTI_IRQn EXTI9_5_IRQn
#define DISP_A0_Pin GPIO_PIN_8
#define DISP_A0_GPIO_Port GPIOA
#define POT3_CHA_Pin GPIO_PIN_10
#define POT3_CHA_GPIO_Port GPIOA
#define POT3_CHA_EXTI_IRQn EXTI15_10_IRQn
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define DISP_CS_Pin GPIO_PIN_4
#define DISP_CS_GPIO_Port GPIOB
#define POT3_CHB_Pin GPIO_PIN_5
#define POT3_CHB_GPIO_Port GPIOB
#define POT2_CHB_Pin GPIO_PIN_6
#define POT2_CHB_GPIO_Port GPIOB
#define POT1_CHB_Pin GPIO_PIN_8
#define POT1_CHB_GPIO_Port GPIOB
#define POT1_CHA_Pin GPIO_PIN_9
#define POT1_CHA_GPIO_Port GPIOB
#define POT1_CHA_EXTI_IRQn EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
