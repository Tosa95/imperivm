################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/gui/lvgl/src/lv_misc/lv_anim.c \
../Src/gui/lvgl/src/lv_misc/lv_area.c \
../Src/gui/lvgl/src/lv_misc/lv_async.c \
../Src/gui/lvgl/src/lv_misc/lv_circ.c \
../Src/gui/lvgl/src/lv_misc/lv_color.c \
../Src/gui/lvgl/src/lv_misc/lv_fs.c \
../Src/gui/lvgl/src/lv_misc/lv_gc.c \
../Src/gui/lvgl/src/lv_misc/lv_ll.c \
../Src/gui/lvgl/src/lv_misc/lv_log.c \
../Src/gui/lvgl/src/lv_misc/lv_math.c \
../Src/gui/lvgl/src/lv_misc/lv_mem.c \
../Src/gui/lvgl/src/lv_misc/lv_task.c \
../Src/gui/lvgl/src/lv_misc/lv_templ.c \
../Src/gui/lvgl/src/lv_misc/lv_txt.c \
../Src/gui/lvgl/src/lv_misc/lv_utils.c 

OBJS += \
./Src/gui/lvgl/src/lv_misc/lv_anim.o \
./Src/gui/lvgl/src/lv_misc/lv_area.o \
./Src/gui/lvgl/src/lv_misc/lv_async.o \
./Src/gui/lvgl/src/lv_misc/lv_circ.o \
./Src/gui/lvgl/src/lv_misc/lv_color.o \
./Src/gui/lvgl/src/lv_misc/lv_fs.o \
./Src/gui/lvgl/src/lv_misc/lv_gc.o \
./Src/gui/lvgl/src/lv_misc/lv_ll.o \
./Src/gui/lvgl/src/lv_misc/lv_log.o \
./Src/gui/lvgl/src/lv_misc/lv_math.o \
./Src/gui/lvgl/src/lv_misc/lv_mem.o \
./Src/gui/lvgl/src/lv_misc/lv_task.o \
./Src/gui/lvgl/src/lv_misc/lv_templ.o \
./Src/gui/lvgl/src/lv_misc/lv_txt.o \
./Src/gui/lvgl/src/lv_misc/lv_utils.o 

C_DEPS += \
./Src/gui/lvgl/src/lv_misc/lv_anim.d \
./Src/gui/lvgl/src/lv_misc/lv_area.d \
./Src/gui/lvgl/src/lv_misc/lv_async.d \
./Src/gui/lvgl/src/lv_misc/lv_circ.d \
./Src/gui/lvgl/src/lv_misc/lv_color.d \
./Src/gui/lvgl/src/lv_misc/lv_fs.d \
./Src/gui/lvgl/src/lv_misc/lv_gc.d \
./Src/gui/lvgl/src/lv_misc/lv_ll.d \
./Src/gui/lvgl/src/lv_misc/lv_log.d \
./Src/gui/lvgl/src/lv_misc/lv_math.d \
./Src/gui/lvgl/src/lv_misc/lv_mem.d \
./Src/gui/lvgl/src/lv_misc/lv_task.d \
./Src/gui/lvgl/src/lv_misc/lv_templ.d \
./Src/gui/lvgl/src/lv_misc/lv_txt.d \
./Src/gui/lvgl/src/lv_misc/lv_utils.d 


# Each subdirectory must supply rules for building sources it contributes
Src/gui/lvgl/src/lv_misc/lv_anim.o: ../Src/gui/lvgl/src/lv_misc/lv_anim.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_anim.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_area.o: ../Src/gui/lvgl/src/lv_misc/lv_area.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_area.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_async.o: ../Src/gui/lvgl/src/lv_misc/lv_async.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_async.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_circ.o: ../Src/gui/lvgl/src/lv_misc/lv_circ.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_circ.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_color.o: ../Src/gui/lvgl/src/lv_misc/lv_color.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_color.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_fs.o: ../Src/gui/lvgl/src/lv_misc/lv_fs.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_fs.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_gc.o: ../Src/gui/lvgl/src/lv_misc/lv_gc.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_gc.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_ll.o: ../Src/gui/lvgl/src/lv_misc/lv_ll.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_ll.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_log.o: ../Src/gui/lvgl/src/lv_misc/lv_log.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_log.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_math.o: ../Src/gui/lvgl/src/lv_misc/lv_math.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_math.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_mem.o: ../Src/gui/lvgl/src/lv_misc/lv_mem.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_mem.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_task.o: ../Src/gui/lvgl/src/lv_misc/lv_task.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_task.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_templ.o: ../Src/gui/lvgl/src/lv_misc/lv_templ.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_templ.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_txt.o: ../Src/gui/lvgl/src/lv_misc/lv_txt.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_txt.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_misc/lv_utils.o: ../Src/gui/lvgl/src/lv_misc/lv_utils.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_misc/lv_utils.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

