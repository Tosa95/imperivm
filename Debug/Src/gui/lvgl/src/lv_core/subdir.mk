################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/gui/lvgl/src/lv_core/lv_disp.c \
../Src/gui/lvgl/src/lv_core/lv_group.c \
../Src/gui/lvgl/src/lv_core/lv_indev.c \
../Src/gui/lvgl/src/lv_core/lv_obj.c \
../Src/gui/lvgl/src/lv_core/lv_refr.c \
../Src/gui/lvgl/src/lv_core/lv_style.c 

OBJS += \
./Src/gui/lvgl/src/lv_core/lv_disp.o \
./Src/gui/lvgl/src/lv_core/lv_group.o \
./Src/gui/lvgl/src/lv_core/lv_indev.o \
./Src/gui/lvgl/src/lv_core/lv_obj.o \
./Src/gui/lvgl/src/lv_core/lv_refr.o \
./Src/gui/lvgl/src/lv_core/lv_style.o 

C_DEPS += \
./Src/gui/lvgl/src/lv_core/lv_disp.d \
./Src/gui/lvgl/src/lv_core/lv_group.d \
./Src/gui/lvgl/src/lv_core/lv_indev.d \
./Src/gui/lvgl/src/lv_core/lv_obj.d \
./Src/gui/lvgl/src/lv_core/lv_refr.d \
./Src/gui/lvgl/src/lv_core/lv_style.d 


# Each subdirectory must supply rules for building sources it contributes
Src/gui/lvgl/src/lv_core/lv_disp.o: ../Src/gui/lvgl/src/lv_core/lv_disp.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_core/lv_disp.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_core/lv_group.o: ../Src/gui/lvgl/src/lv_core/lv_group.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_core/lv_group.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_core/lv_indev.o: ../Src/gui/lvgl/src/lv_core/lv_indev.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_core/lv_indev.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_core/lv_obj.o: ../Src/gui/lvgl/src/lv_core/lv_obj.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_core/lv_obj.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_core/lv_refr.o: ../Src/gui/lvgl/src/lv_core/lv_refr.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_core/lv_refr.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_core/lv_style.o: ../Src/gui/lvgl/src/lv_core/lv_style.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_core/lv_style.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

