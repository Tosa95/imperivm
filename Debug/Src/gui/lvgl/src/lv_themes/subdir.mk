################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/gui/lvgl/src/lv_themes/lv_theme.c \
../Src/gui/lvgl/src/lv_themes/lv_theme_alien.c \
../Src/gui/lvgl/src/lv_themes/lv_theme_default.c \
../Src/gui/lvgl/src/lv_themes/lv_theme_material.c \
../Src/gui/lvgl/src/lv_themes/lv_theme_mono.c \
../Src/gui/lvgl/src/lv_themes/lv_theme_nemo.c \
../Src/gui/lvgl/src/lv_themes/lv_theme_night.c \
../Src/gui/lvgl/src/lv_themes/lv_theme_templ.c \
../Src/gui/lvgl/src/lv_themes/lv_theme_zen.c 

OBJS += \
./Src/gui/lvgl/src/lv_themes/lv_theme.o \
./Src/gui/lvgl/src/lv_themes/lv_theme_alien.o \
./Src/gui/lvgl/src/lv_themes/lv_theme_default.o \
./Src/gui/lvgl/src/lv_themes/lv_theme_material.o \
./Src/gui/lvgl/src/lv_themes/lv_theme_mono.o \
./Src/gui/lvgl/src/lv_themes/lv_theme_nemo.o \
./Src/gui/lvgl/src/lv_themes/lv_theme_night.o \
./Src/gui/lvgl/src/lv_themes/lv_theme_templ.o \
./Src/gui/lvgl/src/lv_themes/lv_theme_zen.o 

C_DEPS += \
./Src/gui/lvgl/src/lv_themes/lv_theme.d \
./Src/gui/lvgl/src/lv_themes/lv_theme_alien.d \
./Src/gui/lvgl/src/lv_themes/lv_theme_default.d \
./Src/gui/lvgl/src/lv_themes/lv_theme_material.d \
./Src/gui/lvgl/src/lv_themes/lv_theme_mono.d \
./Src/gui/lvgl/src/lv_themes/lv_theme_nemo.d \
./Src/gui/lvgl/src/lv_themes/lv_theme_night.d \
./Src/gui/lvgl/src/lv_themes/lv_theme_templ.d \
./Src/gui/lvgl/src/lv_themes/lv_theme_zen.d 


# Each subdirectory must supply rules for building sources it contributes
Src/gui/lvgl/src/lv_themes/lv_theme.o: ../Src/gui/lvgl/src/lv_themes/lv_theme.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_themes/lv_theme_alien.o: ../Src/gui/lvgl/src/lv_themes/lv_theme_alien.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme_alien.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_themes/lv_theme_default.o: ../Src/gui/lvgl/src/lv_themes/lv_theme_default.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme_default.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_themes/lv_theme_material.o: ../Src/gui/lvgl/src/lv_themes/lv_theme_material.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme_material.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_themes/lv_theme_mono.o: ../Src/gui/lvgl/src/lv_themes/lv_theme_mono.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme_mono.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_themes/lv_theme_nemo.o: ../Src/gui/lvgl/src/lv_themes/lv_theme_nemo.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme_nemo.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_themes/lv_theme_night.o: ../Src/gui/lvgl/src/lv_themes/lv_theme_night.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme_night.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_themes/lv_theme_templ.o: ../Src/gui/lvgl/src/lv_themes/lv_theme_templ.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme_templ.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_themes/lv_theme_zen.o: ../Src/gui/lvgl/src/lv_themes/lv_theme_zen.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_themes/lv_theme_zen.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

