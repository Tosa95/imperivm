################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/gui/lvgl/src/lv_draw/lv_draw.c \
../Src/gui/lvgl/src/lv_draw/lv_draw_arc.c \
../Src/gui/lvgl/src/lv_draw/lv_draw_basic.c \
../Src/gui/lvgl/src/lv_draw/lv_draw_img.c \
../Src/gui/lvgl/src/lv_draw/lv_draw_label.c \
../Src/gui/lvgl/src/lv_draw/lv_draw_line.c \
../Src/gui/lvgl/src/lv_draw/lv_draw_rect.c \
../Src/gui/lvgl/src/lv_draw/lv_draw_triangle.c \
../Src/gui/lvgl/src/lv_draw/lv_img_cache.c \
../Src/gui/lvgl/src/lv_draw/lv_img_decoder.c 

OBJS += \
./Src/gui/lvgl/src/lv_draw/lv_draw.o \
./Src/gui/lvgl/src/lv_draw/lv_draw_arc.o \
./Src/gui/lvgl/src/lv_draw/lv_draw_basic.o \
./Src/gui/lvgl/src/lv_draw/lv_draw_img.o \
./Src/gui/lvgl/src/lv_draw/lv_draw_label.o \
./Src/gui/lvgl/src/lv_draw/lv_draw_line.o \
./Src/gui/lvgl/src/lv_draw/lv_draw_rect.o \
./Src/gui/lvgl/src/lv_draw/lv_draw_triangle.o \
./Src/gui/lvgl/src/lv_draw/lv_img_cache.o \
./Src/gui/lvgl/src/lv_draw/lv_img_decoder.o 

C_DEPS += \
./Src/gui/lvgl/src/lv_draw/lv_draw.d \
./Src/gui/lvgl/src/lv_draw/lv_draw_arc.d \
./Src/gui/lvgl/src/lv_draw/lv_draw_basic.d \
./Src/gui/lvgl/src/lv_draw/lv_draw_img.d \
./Src/gui/lvgl/src/lv_draw/lv_draw_label.d \
./Src/gui/lvgl/src/lv_draw/lv_draw_line.d \
./Src/gui/lvgl/src/lv_draw/lv_draw_rect.d \
./Src/gui/lvgl/src/lv_draw/lv_draw_triangle.d \
./Src/gui/lvgl/src/lv_draw/lv_img_cache.d \
./Src/gui/lvgl/src/lv_draw/lv_img_decoder.d 


# Each subdirectory must supply rules for building sources it contributes
Src/gui/lvgl/src/lv_draw/lv_draw.o: ../Src/gui/lvgl/src/lv_draw/lv_draw.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_draw.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_draw_arc.o: ../Src/gui/lvgl/src/lv_draw/lv_draw_arc.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_draw_arc.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_draw_basic.o: ../Src/gui/lvgl/src/lv_draw/lv_draw_basic.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_draw_basic.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_draw_img.o: ../Src/gui/lvgl/src/lv_draw/lv_draw_img.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_draw_img.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_draw_label.o: ../Src/gui/lvgl/src/lv_draw/lv_draw_label.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_draw_label.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_draw_line.o: ../Src/gui/lvgl/src/lv_draw/lv_draw_line.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_draw_line.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_draw_rect.o: ../Src/gui/lvgl/src/lv_draw/lv_draw_rect.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_draw_rect.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_draw_triangle.o: ../Src/gui/lvgl/src/lv_draw/lv_draw_triangle.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_draw_triangle.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_img_cache.o: ../Src/gui/lvgl/src/lv_draw/lv_img_cache.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_img_cache.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Src/gui/lvgl/src/lv_draw/lv_img_decoder.o: ../Src/gui/lvgl/src/lv_draw/lv_img_decoder.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F446xx -DDEBUG -c -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Inc -I../Drivers/CMSIS/Include -I"C:/Users/mrizz/Desktop/Workspaces/NUCLEO/STM32cube_workspace/Imperivm/CMSIS/CMSIS/DSP/Include" -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -O0 -ffunction-sections -fdata-sections -Wall -Wl,-u,vfprintf -lprintf_flt -lm -fstack-usage -MMD -MP -MF"Src/gui/lvgl/src/lv_draw/lv_img_decoder.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

